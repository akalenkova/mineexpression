﻿namespace Project
{
    partial class Draw
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Draw));
            this.buttonVertex = new System.Windows.Forms.Button();
            this.buttonEdge = new System.Windows.Forms.Button();
            this.pictureBoxAutomata = new System.Windows.Forms.PictureBox();
            this.labelAddVertex = new System.Windows.Forms.Label();
            this.labelAddEdge = new System.Windows.Forms.Label();
            this.labelHelpAdd = new System.Windows.Forms.Label();
            this.buttonStartingVertex = new System.Windows.Forms.Button();
            this.buttonAddFinal = new System.Windows.Forms.Button();
            this.buttonFinish = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAutomata)).BeginInit();
            this.panel1.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonVertex
            // 
            this.buttonVertex.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonVertex.Location = new System.Drawing.Point(34, 232);
            this.buttonVertex.Name = "buttonVertex";
            this.buttonVertex.Size = new System.Drawing.Size(96, 96);
            this.buttonVertex.TabIndex = 0;
            this.buttonVertex.UseVisualStyleBackColor = false;
            this.buttonVertex.Click += new System.EventHandler(this.buttonVertex_Click);
            this.buttonVertex.MouseEnter += new System.EventHandler(this.buttonVertex_MouseEnter);
            // 
            // buttonEdge
            // 
            this.buttonEdge.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonEdge.Location = new System.Drawing.Point(34, 403);
            this.buttonEdge.Name = "buttonEdge";
            this.buttonEdge.Size = new System.Drawing.Size(96, 96);
            this.buttonEdge.TabIndex = 1;
            this.buttonEdge.UseVisualStyleBackColor = false;
            this.buttonEdge.Click += new System.EventHandler(this.buttonEdge_Click);
            this.buttonEdge.MouseEnter += new System.EventHandler(this.buttonEdge_MouseEnter);
            // 
            // pictureBoxAutomata
            // 
            this.pictureBoxAutomata.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBoxAutomata.Location = new System.Drawing.Point(222, 170);
            this.pictureBoxAutomata.Name = "pictureBoxAutomata";
            this.pictureBoxAutomata.Size = new System.Drawing.Size(1474, 1046);
            this.pictureBoxAutomata.TabIndex = 2;
            this.pictureBoxAutomata.TabStop = false;
            this.pictureBoxAutomata.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxAutomata_MouseClick);
            this.pictureBoxAutomata.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxAutomata_MouseDown);
            this.pictureBoxAutomata.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxAutomata_MouseMove_2);
            this.pictureBoxAutomata.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxAutomata_MouseUp);
            // 
            // labelAddVertex
            // 
            this.labelAddVertex.AutoSize = true;
            this.labelAddVertex.BackColor = System.Drawing.Color.SteelBlue;
            this.labelAddVertex.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddVertex.ForeColor = System.Drawing.Color.White;
            this.labelAddVertex.Location = new System.Drawing.Point(0, 170);
            this.labelAddVertex.Name = "labelAddVertex";
            this.labelAddVertex.Size = new System.Drawing.Size(63, 26);
            this.labelAddVertex.TabIndex = 6;
            this.labelAddVertex.Text = "label3";
            // 
            // labelAddEdge
            // 
            this.labelAddEdge.AutoSize = true;
            this.labelAddEdge.BackColor = System.Drawing.Color.SteelBlue;
            this.labelAddEdge.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);
            this.labelAddEdge.ForeColor = System.Drawing.Color.White;
            this.labelAddEdge.Location = new System.Drawing.Point(0, 357);
            this.labelAddEdge.Name = "labelAddEdge";
            this.labelAddEdge.Size = new System.Drawing.Size(63, 26);
            this.labelAddEdge.TabIndex = 7;
            this.labelAddEdge.Text = "label3";
            // 
            // labelHelpAdd
            // 
            this.labelHelpAdd.AutoSize = true;
            this.labelHelpAdd.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold);
            this.labelHelpAdd.ForeColor = System.Drawing.Color.White;
            this.labelHelpAdd.Location = new System.Drawing.Point(292, 114);
            this.labelHelpAdd.Name = "labelHelpAdd";
            this.labelHelpAdd.Size = new System.Drawing.Size(92, 37);
            this.labelHelpAdd.TabIndex = 11;
            this.labelHelpAdd.Text = "label1";
            // 
            // buttonStartingVertex
            // 
            this.buttonStartingVertex.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonStartingVertex.Font = new System.Drawing.Font("Perpetua Titling MT", 12F, System.Drawing.FontStyle.Bold);
            this.buttonStartingVertex.ForeColor = System.Drawing.Color.DodgerBlue;
            this.buttonStartingVertex.Location = new System.Drawing.Point(12, 586);
            this.buttonStartingVertex.Name = "buttonStartingVertex";
            this.buttonStartingVertex.Size = new System.Drawing.Size(180, 135);
            this.buttonStartingVertex.TabIndex = 12;
            this.buttonStartingVertex.Text = "Select starting vertex";
            this.buttonStartingVertex.UseVisualStyleBackColor = false;
            this.buttonStartingVertex.Click += new System.EventHandler(this.buttonStartingVertex_Click);
            this.buttonStartingVertex.MouseEnter += new System.EventHandler(this.buttonStartingVertex_MouseEnter);
            // 
            // buttonAddFinal
            // 
            this.buttonAddFinal.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAddFinal.Font = new System.Drawing.Font("Perpetua Titling MT", 12F, System.Drawing.FontStyle.Bold);
            this.buttonAddFinal.ForeColor = System.Drawing.Color.DodgerBlue;
            this.buttonAddFinal.Location = new System.Drawing.Point(13, 737);
            this.buttonAddFinal.Name = "buttonAddFinal";
            this.buttonAddFinal.Size = new System.Drawing.Size(180, 135);
            this.buttonAddFinal.TabIndex = 13;
            this.buttonAddFinal.Text = "Modify final state list";
            this.buttonAddFinal.UseVisualStyleBackColor = false;
            this.buttonAddFinal.Click += new System.EventHandler(this.buttonAddFinal_Click);
            this.buttonAddFinal.MouseEnter += new System.EventHandler(this.buttonAddFinal_MouseEnter);
            // 
            // buttonFinish
            // 
            this.buttonFinish.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonFinish.Font = new System.Drawing.Font("Perpetua Titling MT", 12F, System.Drawing.FontStyle.Bold);
            this.buttonFinish.ForeColor = System.Drawing.Color.DodgerBlue;
            this.buttonFinish.Location = new System.Drawing.Point(12, 722);
            this.buttonFinish.Name = "buttonFinish";
            this.buttonFinish.Size = new System.Drawing.Size(179, 89);
            this.buttonFinish.TabIndex = 14;
            this.buttonFinish.Text = "Finish!";
            this.buttonFinish.UseVisualStyleBackColor = false;
            this.buttonFinish.Click += new System.EventHandler(this.buttonFinish_Click);
            this.buttonFinish.MouseEnter += new System.EventHandler(this.buttonFinish_MouseEnter);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SteelBlue;
            this.panel1.Controls.Add(this.buttonFinish);
            this.panel1.Location = new System.Drawing.Point(1, 170);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(215, 1036);
            this.panel1.TabIndex = 15;
            // 
            // menuStripMain
            // 
            this.menuStripMain.AutoSize = false;
            this.menuStripMain.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStripMain.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(2198, 65);
            this.menuStripMain.TabIndex = 16;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.newProjectToolStripMenuItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(155, 61);
            this.newProjectToolStripMenuItem.Text = "New Project";
            this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.newProjectToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.saveToolStripMenuItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(77, 61);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.aboutToolStripMenuItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(92, 61);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // Draw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(2198, 1566);
            this.Controls.Add(this.labelAddVertex);
            this.Controls.Add(this.buttonAddFinal);
            this.Controls.Add(this.buttonStartingVertex);
            this.Controls.Add(this.labelHelpAdd);
            this.Controls.Add(this.labelAddEdge);
            this.Controls.Add(this.pictureBoxAutomata);
            this.Controls.Add(this.buttonEdge);
            this.Controls.Add(this.buttonVertex);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStripMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMain;
            this.MaximizeBox = false;
            this.Name = "Draw";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expression Miner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Draw_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAutomata)).EndInit();
            this.panel1.ResumeLayout(false);
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonVertex;
        private System.Windows.Forms.Button buttonEdge;
        private System.Windows.Forms.PictureBox pictureBoxAutomata;
        private System.Windows.Forms.Label labelAddVertex;
        private System.Windows.Forms.Label labelAddEdge;
        private System.Windows.Forms.Label labelHelpAdd;
        private System.Windows.Forms.Button buttonStartingVertex;
        private System.Windows.Forms.Button buttonAddFinal;
        private System.Windows.Forms.Button buttonFinish;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}