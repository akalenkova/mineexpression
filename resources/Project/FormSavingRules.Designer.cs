﻿namespace Project
{
    partial class FormSavingRules
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSavingRules));
            this.checkBoxAfterAlgo = new System.Windows.Forms.CheckBox();
            this.checkBoxBasics = new System.Windows.Forms.CheckBox();
            this.checkBoxBeforeAlgo = new System.Windows.Forms.CheckBox();
            this.checkBoxProcessDefinitions = new System.Windows.Forms.CheckBox();
            this.checkBoxAnalytics = new System.Windows.Forms.CheckBox();
            this.buttonAddAll = new System.Windows.Forms.Button();
            this.buttonSaveChosen = new System.Windows.Forms.Button();
            this.labelChoose = new System.Windows.Forms.Label();
            this.panelMain = new System.Windows.Forms.Panel();
            this.panelSecond = new System.Windows.Forms.Panel();
            this.panelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBoxAfterAlgo
            // 
            this.checkBoxAfterAlgo.AutoSize = true;
            this.checkBoxAfterAlgo.BackColor = System.Drawing.Color.SteelBlue;
            this.checkBoxAfterAlgo.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.checkBoxAfterAlgo.ForeColor = System.Drawing.Color.White;
            this.checkBoxAfterAlgo.Location = new System.Drawing.Point(221, 141);
            this.checkBoxAfterAlgo.Name = "checkBoxAfterAlgo";
            this.checkBoxAfterAlgo.Size = new System.Drawing.Size(472, 33);
            this.checkBoxAfterAlgo.TabIndex = 0;
            this.checkBoxAfterAlgo.Text = "Expression mined after applying the algorithm";
            this.checkBoxAfterAlgo.UseVisualStyleBackColor = false;
            // 
            // checkBoxBasics
            // 
            this.checkBoxBasics.AutoSize = true;
            this.checkBoxBasics.BackColor = System.Drawing.Color.SteelBlue;
            this.checkBoxBasics.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.checkBoxBasics.ForeColor = System.Drawing.Color.White;
            this.checkBoxBasics.Location = new System.Drawing.Point(221, 195);
            this.checkBoxBasics.Name = "checkBoxBasics";
            this.checkBoxBasics.Size = new System.Drawing.Size(195, 33);
            this.checkBoxBasics.TabIndex = 1;
            this.checkBoxBasics.Text = "Basic definitions";
            this.checkBoxBasics.UseVisualStyleBackColor = false;
            // 
            // checkBoxBeforeAlgo
            // 
            this.checkBoxBeforeAlgo.AutoSize = true;
            this.checkBoxBeforeAlgo.BackColor = System.Drawing.Color.SteelBlue;
            this.checkBoxBeforeAlgo.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.checkBoxBeforeAlgo.ForeColor = System.Drawing.Color.White;
            this.checkBoxBeforeAlgo.Location = new System.Drawing.Point(221, 252);
            this.checkBoxBeforeAlgo.Name = "checkBoxBeforeAlgo";
            this.checkBoxBeforeAlgo.Size = new System.Drawing.Size(498, 33);
            this.checkBoxBeforeAlgo.TabIndex = 2;
            this.checkBoxBeforeAlgo.Text = "Expression mined without applying the algorithm";
            this.checkBoxBeforeAlgo.UseVisualStyleBackColor = false;
            // 
            // checkBoxProcessDefinitions
            // 
            this.checkBoxProcessDefinitions.AutoSize = true;
            this.checkBoxProcessDefinitions.BackColor = System.Drawing.Color.SteelBlue;
            this.checkBoxProcessDefinitions.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.checkBoxProcessDefinitions.ForeColor = System.Drawing.Color.White;
            this.checkBoxProcessDefinitions.Location = new System.Drawing.Point(221, 307);
            this.checkBoxProcessDefinitions.Name = "checkBoxProcessDefinitions";
            this.checkBoxProcessDefinitions.Size = new System.Drawing.Size(220, 33);
            this.checkBoxProcessDefinitions.TabIndex = 3;
            this.checkBoxProcessDefinitions.Text = "Process definitions";
            this.checkBoxProcessDefinitions.UseVisualStyleBackColor = false;
            // 
            // checkBoxAnalytics
            // 
            this.checkBoxAnalytics.AutoSize = true;
            this.checkBoxAnalytics.BackColor = System.Drawing.Color.SteelBlue;
            this.checkBoxAnalytics.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.checkBoxAnalytics.ForeColor = System.Drawing.Color.White;
            this.checkBoxAnalytics.Location = new System.Drawing.Point(221, 365);
            this.checkBoxAnalytics.Name = "checkBoxAnalytics";
            this.checkBoxAnalytics.Size = new System.Drawing.Size(126, 33);
            this.checkBoxAnalytics.TabIndex = 4;
            this.checkBoxAnalytics.Text = "Analytics";
            this.checkBoxAnalytics.UseVisualStyleBackColor = false;
            // 
            // buttonAddAll
            // 
            this.buttonAddAll.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAddAll.Font = new System.Drawing.Font("Perpetua Titling MT", 12F, System.Drawing.FontStyle.Bold);
            this.buttonAddAll.ForeColor = System.Drawing.Color.DodgerBlue;
            this.buttonAddAll.Location = new System.Drawing.Point(190, 506);
            this.buttonAddAll.Name = "buttonAddAll";
            this.buttonAddAll.Size = new System.Drawing.Size(181, 89);
            this.buttonAddAll.TabIndex = 5;
            this.buttonAddAll.Text = "Save All";
            this.buttonAddAll.UseVisualStyleBackColor = false;
            this.buttonAddAll.Click += new System.EventHandler(this.buttonAddAll_Click);
            // 
            // buttonSaveChosen
            // 
            this.buttonSaveChosen.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSaveChosen.Font = new System.Drawing.Font("Perpetua Titling MT", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSaveChosen.ForeColor = System.Drawing.Color.DodgerBlue;
            this.buttonSaveChosen.Location = new System.Drawing.Point(551, 506);
            this.buttonSaveChosen.Name = "buttonSaveChosen";
            this.buttonSaveChosen.Size = new System.Drawing.Size(181, 89);
            this.buttonSaveChosen.TabIndex = 6;
            this.buttonSaveChosen.Text = "Save";
            this.buttonSaveChosen.UseVisualStyleBackColor = false;
            this.buttonSaveChosen.Click += new System.EventHandler(this.buttonSaveChosen_Click);
            // 
            // labelChoose
            // 
            this.labelChoose.AutoSize = true;
            this.labelChoose.BackColor = System.Drawing.Color.SteelBlue;
            this.labelChoose.Font = new System.Drawing.Font("Perpetua Titling MT", 14F, System.Drawing.FontStyle.Bold);
            this.labelChoose.ForeColor = System.Drawing.Color.White;
            this.labelChoose.Location = new System.Drawing.Point(144, 40);
            this.labelChoose.Name = "labelChoose";
            this.labelChoose.Size = new System.Drawing.Size(625, 33);
            this.labelChoose.TabIndex = 7;
            this.labelChoose.Text = "Information that should be saved:";
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.SteelBlue;
            this.panelMain.Controls.Add(this.labelChoose);
            this.panelMain.Location = new System.Drawing.Point(1, 2);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(920, 100);
            this.panelMain.TabIndex = 8;
            // 
            // panelSecond
            // 
            this.panelSecond.BackColor = System.Drawing.Color.SteelBlue;
            this.panelSecond.Location = new System.Drawing.Point(190, 95);
            this.panelSecond.Name = "panelSecond";
            this.panelSecond.Size = new System.Drawing.Size(542, 386);
            this.panelSecond.TabIndex = 9;
            // 
            // FormSavingRules
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(919, 681);
            this.Controls.Add(this.buttonSaveChosen);
            this.Controls.Add(this.buttonAddAll);
            this.Controls.Add(this.checkBoxAnalytics);
            this.Controls.Add(this.checkBoxProcessDefinitions);
            this.Controls.Add(this.checkBoxBeforeAlgo);
            this.Controls.Add(this.checkBoxBasics);
            this.Controls.Add(this.checkBoxAfterAlgo);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.panelSecond);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "FormSavingRules";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expression Miner";
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxAfterAlgo;
        private System.Windows.Forms.CheckBox checkBoxBasics;
        private System.Windows.Forms.CheckBox checkBoxBeforeAlgo;
        private System.Windows.Forms.CheckBox checkBoxProcessDefinitions;
        private System.Windows.Forms.CheckBox checkBoxAnalytics;
        private System.Windows.Forms.Button buttonAddAll;
        private System.Windows.Forms.Button buttonSaveChosen;
        private System.Windows.Forms.Label labelChoose;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel panelSecond;
    }
}