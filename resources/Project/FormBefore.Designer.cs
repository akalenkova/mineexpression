﻿namespace Project
{
    partial class FormBefore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBefore));
            this.richTextBoxBefore = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelNumOfEvents = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelMaxNesting = new System.Windows.Forms.Label();
            this.panelMain = new System.Windows.Forms.Panel();
            this.panelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBoxBefore
            // 
            this.richTextBoxBefore.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.richTextBoxBefore.ForeColor = System.Drawing.Color.SteelBlue;
            this.richTextBoxBefore.Location = new System.Drawing.Point(21, 240);
            this.richTextBoxBefore.Name = "richTextBoxBefore";
            this.richTextBoxBefore.ReadOnly = true;
            this.richTextBoxBefore.Size = new System.Drawing.Size(1641, 912);
            this.richTextBoxBefore.TabIndex = 0;
            this.richTextBoxBefore.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(20, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(812, 37);
            this.label1.TabIndex = 1;
            this.label1.Text = "Length of the regular expression before applying the algorithm:";
            // 
            // labelNumOfEvents
            // 
            this.labelNumOfEvents.AutoSize = true;
            this.labelNumOfEvents.Font = new System.Drawing.Font("Arial Narrow", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.labelNumOfEvents.ForeColor = System.Drawing.Color.White;
            this.labelNumOfEvents.Location = new System.Drawing.Point(513, 101);
            this.labelNumOfEvents.Name = "labelNumOfEvents";
            this.labelNumOfEvents.Size = new System.Drawing.Size(92, 37);
            this.labelNumOfEvents.TabIndex = 2;
            this.labelNumOfEvents.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(1125, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(248, 37);
            this.label2.TabIndex = 3;
            this.label2.Text = "Maximum nesting:";
            // 
            // labelMaxNesting
            // 
            this.labelMaxNesting.AutoSize = true;
            this.labelMaxNesting.Font = new System.Drawing.Font("Arial Narrow", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.labelMaxNesting.ForeColor = System.Drawing.Color.White;
            this.labelMaxNesting.Location = new System.Drawing.Point(1268, 101);
            this.labelMaxNesting.Name = "labelMaxNesting";
            this.labelMaxNesting.Size = new System.Drawing.Size(92, 37);
            this.labelMaxNesting.TabIndex = 4;
            this.labelMaxNesting.Text = "label3";
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.SteelBlue;
            this.panelMain.Controls.Add(this.labelMaxNesting);
            this.panelMain.Controls.Add(this.label1);
            this.panelMain.Controls.Add(this.label2);
            this.panelMain.Controls.Add(this.labelNumOfEvents);
            this.panelMain.Location = new System.Drawing.Point(21, 50);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1641, 199);
            this.panelMain.TabIndex = 5;
            // 
            // FormBefore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1690, 1197);
            this.Controls.Add(this.richTextBoxBefore);
            this.Controls.Add(this.panelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormBefore";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expression Miner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormBefore_FormClosing);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxBefore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelNumOfEvents;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelMaxNesting;
        private System.Windows.Forms.Panel panelMain;
    }
}