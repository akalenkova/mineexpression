﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DrawingSupporter;
using System.IO;
using LibGraph;

namespace Project
{
    /// <summary>
    /// Класс, представляющий окно с инструментами и полем для создания пользовательского конечного автомата.
    /// </summary>
    public partial class Draw : Form
    {
        /// <summary>
        /// Инструмент, оперирующий операциями отрисовки.
        /// </summary>
        DrawAssistant drawer;
        /// <summary>
        /// Построенный граф, представляющий конечный автомат.
        /// </summary>
        GraphBuilder graph;
        /// <summary>
        /// Истинно, если происходит добавление состояний.
        /// </summary>
        bool addingVertices;
        /// <summary>
        /// Истинно, если происходит добавление переходов.
        /// </summary>
        bool addingEdges;
        /// <summary>
        /// Истинно, если происходит передвижение вершины.
        /// </summary>
        bool movingVertex;
        /// <summary>
        /// Истинно, если происходит выбор начального состояния.
        /// </summary>
        bool selectingStart;
        /// <summary>
        /// Истинно, если происходит изменение списка конечных вершин.
        /// </summary>
        bool correctingFinalsList;
        /// <summary>
        /// Имя текущего передвигаемого состояния.
        /// </summary>
        string moovingName = "";
        //Константные значения текстовых полей.
        const string textAddVert1 = "Start adding vertices";
        const string textAddVert2 = "Stop adding vertices";
        const string textAddEdge1 = "Start adding edges";
        const string textAddEdge2 = "Stop adding edges";
        //Границы, в пределах которых можно перемещать объекты.
        int leftBound;
        int rightBound;
        int upperBound;
        int lowerBound;
        /// <summary>
        /// Вершина, из которой исходит ребро.
        /// </summary>
        Vertex sourceVertex;
        /// <summary>
        /// Вершина, в которую входит ребро.
        /// </summary>
        Vertex targetVertex;
        /// <summary>
        /// Перемещаемая вершина.
        /// </summary>
        Vertex movedVertex;
        /// <summary>
        /// Список вершин.
        /// </summary>
        List<Vertex> vertexList;
        /// <summary>
        /// Список ребер.
        /// </summary>
        List<Edge> edgeList;
        /// <summary>
        /// Список ребер, которые нужно переместить.
        /// </summary>
        List<Edge> movingEdgeList;
        /// <summary>
        /// Список конечных состояний.
        /// </summary>
        List<string> finalsList;
        /// <summary>
        /// Начальное состояние.
        /// </summary>
        string startingState;
        /// <summary>
        /// Стартовое окно.
        /// </summary>
        ChoiceFSMForm newProjectForm;
        public Draw(ChoiceFSMForm form)
        {
            InitializeComponent();
            newProjectForm = form;
            finalsList = new List<string>();
            vertexList = new List<Vertex>();
            edgeList = new List<Edge>();
            movingEdgeList = new List<Edge>();
            drawer = new DrawAssistant(pictureBoxAutomata.Width, pictureBoxAutomata.Height,edgeList,vertexList, finalsList);
            pictureBoxAutomata.Image = drawer.Image;
            Bitmap bit = new Bitmap(Image.FromStream(new MemoryStream(File.ReadAllBytes("circle1.jpg"))), new Size(10 * buttonVertex.Size.Width / 12, 10 * buttonVertex.Size.Width / 12));
            buttonVertex.Image= bit;
            bit = new Bitmap(Image.FromStream(new MemoryStream(File.ReadAllBytes("transition.jpg"))), new Size(10 * buttonEdge.Size.Width / 12, 10 * buttonEdge.Size.Width / 12));
            buttonEdge.Image = bit;
            moovingName = "";
            startingState = "";
            addingVertices = false;
            movingVertex = false;
            addingEdges = false;
            selectingStart = false;
            correctingFinalsList = false;
            labelAddEdge.Text = textAddEdge1;
            labelAddVertex.Text = textAddVert1;
            labelHelpAdd.Text = "";
            leftBound = drawer.Radius;
            rightBound = pictureBoxAutomata.Width- drawer.Radius;
            lowerBound = drawer.Radius;
            upperBound = pictureBoxAutomata.Height - drawer.Radius;
            Size = new Size( pictureBoxAutomata.Location.X + pictureBoxAutomata.Width,pictureBoxAutomata.Location.Y + pictureBoxAutomata.Height);
        }
        /// <summary>
        /// Обработка желания пользователя добавить вершину.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonVertex_Click(object sender, EventArgs e)
        {
            drawer.Draw();
            pictureBoxAutomata.Image = drawer.Image;
            addingVertices = !addingVertices;
            if (addingVertices)
            {
                labelHelpAdd.Text = "Adding vertices...";
                labelAddVertex.Text = textAddVert2;
            }
            else
            {
                labelAddVertex.Text = textAddVert1;
                labelHelpAdd.Text = "";
            }
            movingVertex = false;
            addingEdges = false;
            selectingStart = false;
            correctingFinalsList = false;
            labelAddEdge.Text = textAddEdge1;
        }
        /// <summary>
        /// Обработка желания пользователя добавить ребро.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEdge_Click(object sender, EventArgs e)
        {
            drawer.Draw();
            pictureBoxAutomata.Image = drawer.Image;
            addingVertices = false;
            movingVertex = false;
            selectingStart = false;
            correctingFinalsList = false;
            labelAddVertex.Text = textAddVert1;
            addingEdges = !addingEdges;
            if (addingEdges)
            {
                labelHelpAdd.Text = "Please, choose source vertex...";
                labelAddEdge.Text = textAddEdge2;
            }
            else
            {
                labelAddEdge.Text = textAddEdge1;
                labelHelpAdd.Text = "";
            }
        }
        /// <summary>
        /// Обработка нажатия на поле для рисования. Выполняет операции рисования в зависимости от того, какое действие выполняет пользователь в данный момент.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBoxAutomata_MouseClick(object sender, MouseEventArgs e)
        {
            Vertex vertex = null;
            if (e.Button == MouseButtons.Left)
            {
                if (!movingVertex && !addingEdges)
                {
                    if (addingVertices)//если выбрали место для добавления состояния, рисуем состояние
                    {
                        int x, y;
                        SetXY(out x, out y, e);
                        string name = (vertexList.Count + 1).ToString();
                        drawer.DrawVertex(x, y, name);
                        vertexList.Add(new Vertex(name, x, y));
                        pictureBoxAutomata.Image = drawer.Image;
                    }
                    if (selectingStart && drawer.VertexHitten(ref vertex, e.X, e.Y, vertexList))
                    {
                       startingState= drawer.StartingState = vertex.Name;
                        drawer.Draw();
                        pictureBoxAutomata.Image = drawer.Image;
                    }
                    if (correctingFinalsList && drawer.VertexHitten(ref vertex, e.X, e.Y, vertexList))
                    {
                        drawer.ChangeFinalsList(vertex.Name);
                        drawer.Draw();
                        pictureBoxAutomata.Image = drawer.Image;
                    }
                    if (!selectingStart&&!correctingFinalsList&&!addingVertices && drawer.VertexHitten(ref vertex, e.X, e.Y, vertexList))//начали движение состояния
                    {
                        movingVertex = true;
                        vertexList.Remove(vertex);
                        drawer.Draw();
                        moovingName = vertex.Name;
                    }

                    }
                if (addingEdges && drawer.VertexHitten(ref vertex, e.X, e.Y, vertexList))//выбрали связующее состояние
                {
                    if (sourceVertex == null)
                    {
                        sourceVertex = vertex;
                        drawer.DrawSelectedVertex(vertex.X,vertex.Y,vertex.Name);
                        pictureBoxAutomata.Image = drawer.Image;
                        labelHelpAdd.Text = "Please, choose target vertex...";
                    }
                    else
                    {
                        targetVertex = vertex;
                        labelHelpAdd.Text = "";
                        FormTransitionName form = new FormTransitionName(this);
                        form.Show();
                    }
                }
            }
            else
            {
                Edge edge=null;
                DialogResult dia = DialogResult.No;
                if (drawer.VertexHitten(ref vertex, e.X, e.Y, vertexList))
                {
                    drawer.DrawVertexToDelete(vertex.X, vertex.Y, vertex.Name);
                    pictureBoxAutomata.Image = drawer.Image;
                    dia = MessageBox.Show("Delete vertex " + vertex.Name + "?", "Sure", MessageBoxButtons.YesNo);
                }
                if (dia == DialogResult.Yes)
                {
                    drawer.DeleteVertex(vertex);
                    pictureBoxAutomata.Image = drawer.Image;
                }
                if(drawer.EdgeHitten(ref edge,e.X,e.Y))
                    dia = MessageBox.Show("Delete edge " + edge.Transition + "?", "Sure", MessageBoxButtons.YesNo);
                if (dia == DialogResult.Yes)
                {
                    drawer.DeleteEdge(edge);
                    pictureBoxAutomata.Image = drawer.Image;
                }
            }
        }
        /// <summary>
        /// Завершение добавление ребра после введения текста перехода.
        /// </summary>
        /// <param name="name"></param>
        public void CompleteAddingEdge(string name)
        {
            labelHelpAdd.Text = "Please, choose source vertex...";
            if (name != "")
            {
                bool found=false;
                foreach(Edge edge in edgeList)
                {
                    if(edge.Target==targetVertex&&edge.Source==sourceVertex)
                    {
                        found = true;
                        edge.Transition = edge.Transition + "+" + name;
                        break;
                    }
                }
                if(!found)
                    edgeList.Add(new Edge(sourceVertex, targetVertex, name));
            }
            drawer.Draw();
            pictureBoxAutomata.Image = drawer.Image;
            sourceVertex = null;
        }
        /// <summary>
        /// Метод выполняющий проверку, не пытается ли пользователь переместить объект за поле для рисования.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="e"></param>
        void SetXY(out int x, out int y, MouseEventArgs e)
        {
            if (e.X > leftBound && e.X < rightBound)
                x = e.X;
            else if (e.X <= leftBound)
                x = leftBound;
            else
                x = rightBound;
            if (e.Y > lowerBound && e.Y < upperBound)
                y = e.Y;
            else if (e.Y <= lowerBound)
                y = lowerBound;
            else
                y = upperBound;
        }
        /// <summary>
        /// Метод, обрабатывающий разжатие кнопки мыши. Выполняет операции рисования в зависимости от того, какое действие выполняет пользователь в данный момент.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBoxAutomata_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (movingVertex && !addingEdges && !correctingFinalsList && !selectingStart && !addingVertices)
            {
                int x, y;
                SetXY(out x, out y, e);
                Vertex vertex = new Vertex(movedVertex.Name, x, y);
                vertexList.Add(vertex);
                bool found = false;
                do
                {
                    found = false;
                    foreach (var edge in movingEdgeList)
                        if (edge.Source.Name == movedVertex.Name || edge.Target.Name == movedVertex.Name)
                        {
                            if (edge.Source.Name == movedVertex.Name && (edge.Source.X != x||edge.Source.Y!=y))
                            {
                                found = true;
                                edge.Source = vertex;
                            }
                            if (edge.Target.Name == movedVertex.Name && (edge.Target.X != x||edge.Target.Y!=y))
                            {
                                found = true;
                                edge.Target = vertex;
                            }
                            break;
                        }
                } while (found);
                foreach (var edge in movingEdgeList)
                    edgeList.Add(edge);
                drawer.Draw();
                pictureBoxAutomata.Image = drawer.Image;
                movingEdgeList = new List<Edge>();
                movedVertex = null;
                movingVertex = !movingVertex;
            }
        }

        /// <summary>
        /// Метод, обрабатывающий движение мыши. Выполняет операции рисования в зависимости от того, какое действие выполняет пользователь в данный момент.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBoxAutomata_MouseMove_2(object sender, MouseEventArgs e)
        {
            if (movingVertex&&!addingEdges&&!correctingFinalsList && !selectingStart&&!addingVertices )
            {
                int x, y;
                SetXY(out x, out y, e);
                bool found = false;
                do
                {
                    found = false;
                    foreach (var edge in movingEdgeList)
                        if (edge.Source.Name == movedVertex.Name || edge.Target.Name == movedVertex.Name)
                        {
                            if (edge.Source.Name == movedVertex.Name&& (edge.Source.X != x || edge.Source.Y != y))
                            {
                                found = true;
                                edge.Source.X = x;
                                edge.Source.Y = y;
                            }
                            if (edge.Target.Name == movedVertex.Name&& (edge.Target.X != x || edge.Target.Y != y))
                            {
                                found = true;
                                edge.Target.X = x;
                                edge.Target.Y = y;
                            }
                            break;
                        }
                } while (found);
                movedVertex.X = x;
                movedVertex.Y = y;
                drawer.ClearSheet();
                foreach (Edge edge in movingEdgeList)
                    drawer.DrawEdge(edge.Source.X, edge.Source.Y, edge.Target.X, edge.Target.Y, edge.Transition,edge);
                foreach (Edge edge in edgeList)
                    drawer.DrawEdge(edge.Source.X, edge.Source.Y, edge.Target.X, edge.Target.Y, edge.Transition,edge);
                foreach (Vertex vert in vertexList)
                    drawer.DrawVertex(vert.X, vert.Y, vert.Name);
                drawer.DrawVertex(x, y,movedVertex.Name);
                pictureBoxAutomata.Image = drawer.Image;

            }
        }
        /// <summary>
        /// Метод, обрабатывающий зажатие кнопки мыши. Выполняет операции рисования в зависимости от того, какое действие выполняет пользователь в данный момент.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBoxAutomata_MouseDown(object sender, MouseEventArgs e)
        {
            Vertex vertex = null;
            if (e.Button == MouseButtons.Left)
            {
                if (!correctingFinalsList&&!selectingStart&&!addingEdges && !addingVertices && drawer.VertexHitten(ref vertex, e.X, e.Y, vertexList))
                {
                    movingVertex = true;
                    vertexList.Remove(vertex);
                    movedVertex = vertex;
                    bool found = false;
                    do
                    {
                        found = false;
                        foreach (var edge in edgeList)
                            if (edge.Source.Name == vertex.Name || edge.Target.Name == vertex.Name)
                            {
                                found = true;
                                edgeList.Remove(edge);
                                movingEdgeList.Add(edge);
                                break;
                            }
                    } while (found);
                }
            }
            else
            {
                DialogResult dia = DialogResult.No;
                if (drawer.VertexHitten(ref vertex, e.X, e.Y, vertexList))
                {
                    drawer.DrawVertexToDelete(vertex.X, vertex.Y, vertex.Name);
                    pictureBoxAutomata.Image = drawer.Image;
                    dia = MessageBox.Show("Delete vertex " + vertex.Name + "?", "Sure", MessageBoxButtons.YesNo);
                }
                if (dia == DialogResult.Yes)
                {
                    drawer.DeleteVertex(vertex);
                    pictureBoxAutomata.Image = drawer.Image;
                }
            }
        }
        /// <summary>
        /// Метод, обрабатывающий желание пользователя назначить начальное состояние.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStartingVertex_Click(object sender, EventArgs e)
        {
            drawer.Draw();
            pictureBoxAutomata.Image = drawer.Image;
            selectingStart = !selectingStart;
            if(!selectingStart)
                labelHelpAdd.Text = "";
            else
                labelHelpAdd.Text = "Selecting starting state...";
            addingVertices = false;
            movingVertex = false;
            addingEdges = false;
            correctingFinalsList = false;
            labelAddEdge.Text = textAddEdge1;
            labelAddVertex.Text = textAddVert1;
        }
        /// <summary>
        /// Метод, обрабатывающий желание пользователя отредвктировать список конечных состояний.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddFinal_Click(object sender, EventArgs e)
        {
            drawer.Draw();
            pictureBoxAutomata.Image = drawer.Image;
            correctingFinalsList = !correctingFinalsList;
            if(correctingFinalsList==false)
                labelHelpAdd.Text = "";
            else
                labelHelpAdd.Text = "Correcting finals list...";
            addingVertices = false;
            movingVertex = false;
            addingEdges = false;
            selectingStart = false;
            labelAddEdge.Text = textAddEdge1;
            labelAddVertex.Text = textAddVert1;
        }
        /// <summary>
        /// Метод, обрабатывающий желание пользователя закончить построение автомата.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFinish_Click(object sender, EventArgs e)
        {
            if (startingState == String.Empty)
                MessageBox.Show( "Please, choose a starting state...", "Error!",MessageBoxButtons.OK);
            else if (drawer.Finals.Count == 0)
                MessageBox.Show( "Please, choose at least one final state...", "Error!",MessageBoxButtons.OK);
            else if (edgeList.Count == 0)
                MessageBox.Show("Please, add at least one edge", "Error!",MessageBoxButtons.OK);
            else
            {
                graph = new GraphBuilder();
                foreach(Vertex vertex in vertexList)
                {
                    if (vertex.Name == startingState)
                    {
                        graph.UserAddVertix("");
                        graph.UserAddVertix(vertex.Name);
                        graph.UserAddEdge("", vertex.Name, "");
                    }
                    else if (drawer.Finals.Contains(vertex.Name))
                        graph.UserAddFinalVertix(vertex.Name);
                    else
                        graph.UserAddVertix(vertex.Name);
                }
                foreach (Edge edge in edgeList)
                    graph.UserAddEdge(edge.Source.Name, edge.Target.Name, edge.Transition);
                FormUsersFSM form = new FormUsersFSM(graph, drawer.Finals, startingState,newProjectForm);
                form.Show();
                Hide();
            }

        }
        /// <summary>
        /// Метод, открывающий окно с информацией о разработчике.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInfo formInfo = new FormInfo();
            formInfo.ShowDialog();
        }
        /// <summary>
        /// Метод, открывающий стартовое окно приложения.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = DialogResult.Cancel;
            dialogResult=MessageBox.Show("Are you sure, that you want to start a new project? Current progress will be lost.", "Ensure starting new project", MessageBoxButtons.OKCancel);
            if(dialogResult==DialogResult.OK)
            {
                newProjectForm.Show();
                Hide();
            }
        }
        /// <summary>
        /// Метод, сохраняющий построенный автомат в формате .tsml.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (startingState == String.Empty)
                MessageBox.Show("Please, choose a starting state...", "Error!",MessageBoxButtons.OK);
            else if (drawer.Finals.Count == 0)
                MessageBox.Show( "Please, choose at least one final state...", "Error!",MessageBoxButtons.OK);
            else if (edgeList.Count == 0)
                MessageBox.Show( "Please, add at least one edge", "Error!",MessageBoxButtons.OK);
            else
            {

                TSMLtransformer transformer;
                UsersFiles file = new UsersFiles();
                string path = file.ChooseSavingPath("tsml");
                if (path == String.Empty)
                    MessageBox.Show("Error! File was not saved!");
                else
                transformer = new TSMLtransformer(path, vertexList, finalsList, startingState, edgeList);
            }
        }
        /// <summary>
        /// Метод, обрабатывающий закрытие формы. Предупреждает о возможной потери данных.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Draw_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result;
            if (e.CloseReason!=CloseReason.ApplicationExitCall)
            {
                result = MessageBox.Show("Do you want to quit the application?\nCurrent progress will be lost...", "Ensure quitting the application", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
        //Далее представлены методы, генерирующие появление сплывающих подсказок.
        ToolTip toolTip = new ToolTip();
        private void buttonVertex_MouseEnter(object sender, EventArgs e)
        {
            if(!addingVertices)
                toolTip.SetToolTip(buttonVertex, "Press to start adding vertices...");
            else
                toolTip.SetToolTip(buttonVertex, "Press to stop adding vertices...");
        }

        private void buttonEdge_MouseEnter(object sender, EventArgs e)
        {
            if(!addingEdges)
                toolTip.SetToolTip(buttonEdge, "Press to start adding edges...");
            else
                toolTip.SetToolTip(buttonEdge, "Press to stop adding edges...");
        }

        private void buttonStartingVertex_MouseEnter(object sender, EventArgs e)
        {
            if(!selectingStart)
                toolTip.SetToolTip(buttonStartingVertex, "Press to select a starting state of the FSM...");
            else
                toolTip.SetToolTip(buttonStartingVertex, "Press to stop selecting the starting state...");
        }

        private void buttonAddFinal_MouseEnter(object sender, EventArgs e)
        {
            if(!correctingFinalsList)
                toolTip.SetToolTip(buttonAddFinal, "Press to add or remove a state from the list of final states of the FSM...");
            else
                toolTip.SetToolTip(buttonAddFinal, "Press to stop correcting the list of final states of the FSM...");
        }

        private void buttonFinish_MouseEnter(object sender, EventArgs e)
        {
            toolTip.SetToolTip(buttonFinish, "Press here, if you finished building the FSM...");
        }
    }
}
