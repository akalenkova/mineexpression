﻿namespace Project
{
    partial class FormRegularExpression
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRegularExpression));
            this.labelYourResult = new System.Windows.Forms.Label();
            this.labelRegularExpression = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.richTextBoxRegularExpression = new System.Windows.Forms.RichTextBox();
            this.buttonDefinitions = new System.Windows.Forms.Button();
            this.buttonBasics = new System.Windows.Forms.Button();
            this.buttonBefore = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelNoDic = new System.Windows.Forms.Label();
            this.panelMain = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelMain.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelYourResult
            // 
            this.labelYourResult.AutoSize = true;
            this.labelYourResult.BackColor = System.Drawing.Color.SteelBlue;
            this.labelYourResult.Font = new System.Drawing.Font("Perpetua Titling MT", 16F, System.Drawing.FontStyle.Bold);
            this.labelYourResult.ForeColor = System.Drawing.Color.White;
            this.labelYourResult.Location = new System.Drawing.Point(3, 132);
            this.labelYourResult.Name = "labelYourResult";
            this.labelYourResult.Size = new System.Drawing.Size(537, 38);
            this.labelYourResult.TabIndex = 0;
            this.labelYourResult.Text = "Mined regular expression:";
            // 
            // labelRegularExpression
            // 
            this.labelRegularExpression.AutoSize = true;
            this.labelRegularExpression.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRegularExpression.Location = new System.Drawing.Point(57, 222);
            this.labelRegularExpression.MaximumSize = new System.Drawing.Size(1200, 500);
            this.labelRegularExpression.Name = "labelRegularExpression";
            this.labelRegularExpression.Size = new System.Drawing.Size(0, 29);
            this.labelRegularExpression.TabIndex = 1;
            this.labelRegularExpression.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // richTextBoxRegularExpression
            // 
            this.richTextBoxRegularExpression.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBoxRegularExpression.ForeColor = System.Drawing.Color.SteelBlue;
            this.richTextBoxRegularExpression.Location = new System.Drawing.Point(75, 246);
            this.richTextBoxRegularExpression.Name = "richTextBoxRegularExpression";
            this.richTextBoxRegularExpression.ReadOnly = true;
            this.richTextBoxRegularExpression.Size = new System.Drawing.Size(1433, 821);
            this.richTextBoxRegularExpression.TabIndex = 4;
            this.richTextBoxRegularExpression.Text = "";
            // 
            // buttonDefinitions
            // 
            this.buttonDefinitions.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDefinitions.Font = new System.Drawing.Font("Perpetua Titling MT", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDefinitions.ForeColor = System.Drawing.Color.DodgerBlue;
            this.buttonDefinitions.Location = new System.Drawing.Point(280, 9);
            this.buttonDefinitions.Name = "buttonDefinitions";
            this.buttonDefinitions.Size = new System.Drawing.Size(260, 110);
            this.buttonDefinitions.TabIndex = 5;
            this.buttonDefinitions.Text = "Show subprocess definitions";
            this.buttonDefinitions.UseVisualStyleBackColor = false;
            this.buttonDefinitions.Click += new System.EventHandler(this.buttonDefinitions_Click);
            // 
            // buttonBasics
            // 
            this.buttonBasics.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonBasics.Font = new System.Drawing.Font("Perpetua Titling MT", 12F, System.Drawing.FontStyle.Bold);
            this.buttonBasics.ForeColor = System.Drawing.Color.DodgerBlue;
            this.buttonBasics.Location = new System.Drawing.Point(0, 9);
            this.buttonBasics.Name = "buttonBasics";
            this.buttonBasics.Size = new System.Drawing.Size(260, 110);
            this.buttonBasics.TabIndex = 6;
            this.buttonBasics.Text = "Show Process Definitions";
            this.buttonBasics.UseVisualStyleBackColor = false;
            this.buttonBasics.Click += new System.EventHandler(this.buttonBasics_Click);
            // 
            // buttonBefore
            // 
            this.buttonBefore.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonBefore.Font = new System.Drawing.Font("Perpetua Titling MT", 12F, System.Drawing.FontStyle.Bold);
            this.buttonBefore.ForeColor = System.Drawing.Color.DodgerBlue;
            this.buttonBefore.Location = new System.Drawing.Point(752, 69);
            this.buttonBefore.Name = "buttonBefore";
            this.buttonBefore.Size = new System.Drawing.Size(182, 88);
            this.buttonBefore.TabIndex = 7;
            this.buttonBefore.Text = "Before";
            this.buttonBefore.UseVisualStyleBackColor = false;
            this.buttonBefore.Click += new System.EventHandler(this.buttonBefore_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.SteelBlue;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(610, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(796, 37);
            this.label1.TabIndex = 8;
            this.label1.Text = " Length of the regular expression after applying the algorithm:";
            // 
            // labelNoDic
            // 
            this.labelNoDic.AutoSize = true;
            this.labelNoDic.BackColor = System.Drawing.Color.SteelBlue;
            this.labelNoDic.Font = new System.Drawing.Font("Arial Narrow", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.labelNoDic.ForeColor = System.Drawing.Color.White;
            this.labelNoDic.Location = new System.Drawing.Point(1314, 60);
            this.labelNoDic.Name = "labelNoDic";
            this.labelNoDic.Size = new System.Drawing.Size(92, 37);
            this.labelNoDic.TabIndex = 9;
            this.labelNoDic.Text = "label2";
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.SteelBlue;
            this.panelMain.Controls.Add(this.labelYourResult);
            this.panelMain.Controls.Add(this.labelNoDic);
            this.panelMain.Controls.Add(this.label1);
            this.panelMain.Controls.Add(this.buttonDefinitions);
            this.panelMain.Controls.Add(this.buttonBefore);
            this.panelMain.Controls.Add(this.buttonBasics);
            this.panelMain.Location = new System.Drawing.Point(75, 51);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1433, 200);
            this.panelMain.TabIndex = 10;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1589, 40);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(155, 36);
            this.newProjectToolStripMenuItem.Text = "New Project";
            this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.newProjectToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(77, 36);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(92, 36);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // FormRegularExpression
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1589, 1065);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.richTextBoxRegularExpression);
            this.Controls.Add(this.labelRegularExpression);
            this.Controls.Add(this.panelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "FormRegularExpression";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expression Miner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRegularExpression_FormClosing);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelYourResult;
        private System.Windows.Forms.Label labelRegularExpression;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.RichTextBox richTextBoxRegularExpression;
        private System.Windows.Forms.Button buttonDefinitions;
        private System.Windows.Forms.Button buttonBasics;
        private System.Windows.Forms.Button buttonBefore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelNoDic;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}