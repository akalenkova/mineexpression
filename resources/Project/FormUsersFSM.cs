﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using LibGraph;
using DeletingAlgo;
using System.IO;

namespace Project
{
    /// <summary>
    /// Класс, представляющий окно с возможностью начать построение регулярного выражения с анимацией и без нее.
    /// </summary>
    public partial class FormUsersFSM : Form
    {
        /// <summary>
        /// Конечный автомат, с которым необходимо работать.
        /// </summary>
        GraphBuilder graph;
        /// <summary>
        /// Список из состояний, которые нельзя удалять на начальном этапе.
        /// </summary>
        List<string> untouchedStatesList = new List<string>();
        /// <summary>
        /// Путь к изображению графа.
        /// </summary>
        string imagePath;
        /// <summary>
        /// Начальное состояние автомата.
        /// </summary>
        string headState;
        /// <summary>
        /// Индикатор, была ли успешно начата работа с графом.
        /// </summary>
        bool success;
        /// <summary>
        /// Стартовое окно приложения.
        /// </summary>
        ChoiceFSMForm formChoice;
        /// <summary>
        /// Свойство, предоставляющее доступ к полю success.
        /// </summary>
        public bool Success { get { return success; } }
        /// <summary>
        /// Конструктор вызывается из формы с анимацией, чтобы пропустить дальнейшие шаги анимации.
        /// </summary>
        /// <param name="graph">Текущее состояние графа.</param>
        /// <param name="finalStates">Список конечных состояний.</param>
        /// <param name="headState">Начальное состояние.</param>
        /// <param name="skip">Индикатор, что пропускаются шаги анимации.</param>
        /// <param name="form">Стартовое окно приложения.</param>
        public FormUsersFSM(GraphBuilder graph, List<string> finalStates, string headState, bool skip, ChoiceFSMForm form)
        {
            formChoice = form;
            this.graph = graph;
            untouchedStatesList = finalStates;
            this.headState = headState;
            buttonConstructExpression_Click(new object(), new EventArgs());
        }
        /// <summary>
        /// Конструктор вызывается, когда пользователь только начал работу с автоматом.
        /// </summary>
        /// <param name="graph">Текущее состояние графа.</param>
        /// <param name="finalStates">Список конечных состояний.</param>
        /// <param name="headState">Начальное состояние.</param>
        /// <param name="form">Стартовое окно приложения.</param>
        public FormUsersFSM(GraphBuilder graph, List<string> finalStates,string headState,ChoiceFSMForm form)
        {
            InitializeComponent();
            Height = pictureBoxUsersFSM.Location.Y + pictureBoxUsersFSM.Height + 80;
            formChoice = form;
            graph.InitializeFinalStates(finalStates);
            imagePath = graph.MakeDot(0);
            pictureBoxUsersFSM.Image = Image.FromStream(new MemoryStream(File.ReadAllBytes(imagePath)));
            pictureBoxUsersFSM.SizeMode = PictureBoxSizeMode.Zoom;
            MinimizeGraph(graph, finalStates, headState);
        }
        /// <summary>
        /// Метод, запускающий минимизацию автомата.
        /// </summary>
        /// <param name="graph">Минимизируемый автомат.</param>
        /// <param name="finalStates">Конечные состония автомата.</param>
        /// <param name="headState">Начальное состояние автомата.</param>
        private void MinimizeGraph(GraphBuilder graph, List<string> finalStates, string headState)
        {

            Minimizator minimizator = new Minimizator(finalStates, headState, graph);
            this.graph = minimizator.Minimize();
            if (this.graph != null)
            {
                List<string> verticesToRemove = new List<string>();
                foreach (var vertex in this.graph.myGraph.Vertices)
                {
                    bool found = false;
                    foreach (var edge in this.graph.myGraph.Edges)
                        if (edge.Target == vertex)
                        {
                            found = true;
                            break;
                        }
                    if (!found)
                        verticesToRemove.Add(vertex);
                }
                if (verticesToRemove.Count != 0)
                    foreach (string vertex in verticesToRemove)
                        this.graph.myGraph.RemoveVertex(vertex);
                this.headState = minimizator.StartingState;
                foreach (string state in this.graph.FinalStates)
                    untouchedStatesList.Add(state);
                untouchedStatesList.Add(this.headState);
                untouchedStatesList.Add(String.Empty);
                this.graph.UserAddVertix("");
                this.graph.UserAddEdge("", this.headState, "");
                foreach (var edge in this.graph.myGraph.Edges)
                {
                    if (!this.graph.basicDefinitions.Values.Contains(edge.Tag) && edge.Tag != String.Empty)
                    {
                        int num = this.graph.basicDefinitions.Keys.Count;
                        string str = ((char)(num % 26 + 97)).ToString();
                        if (num >= 26)
                            str += num / 26;
                        this.graph.basicDefinitions.Add(str, edge.Tag);
                    }
                    foreach (var element in this.graph.basicDefinitions)
                        if (element.Value == edge.Tag)
                            edge.Tag = element.Key;
                }
                imagePath = this.graph.MakeDot(-1);
                success = true;
            }
        }
        /// <summary>
        /// Метод, запускающий построение регулярного выражения с анимацией при нажатии соответствующей кнопки.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConstructWithAnimation_Click(object sender, EventArgs e)
        {
            FormRemoveStatesWithAnimation form = new FormRemoveStatesWithAnimation(graph,untouchedStatesList, headState, imagePath, formChoice);
            form.Show();
            Hide();
        }
        /// <summary>
        /// Метод, запускающий построение регулярного выражения без анимации при нажатии соответствующей кнопки.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConstructExpression_Click(object sender, EventArgs e)
        {

            List<string> regularExpression = ConstructRE();
            FormRegularExpression form = new FormRegularExpression(regularExpression, graph,formChoice);
            form.Show();
            Hide();
        }
        /// <summary>
        /// Метод, производящий построение регулярного выражения по конечному автомату без предоставления анимации.
        /// </summary>
        /// <returns>Регулярное выражение.</returns>
        private List<string> ConstructRE()
        {
            StateRemoval state = new StateRemoval(graph, untouchedStatesList, headState, String.Empty);
            string stateName;
            string unqueFinal = String.Empty;//единое конечное состояние
            List<string> listFinals = new List<string>();//список всех допускающих состояний
            foreach (string elem in untouchedStatesList)
                if (elem != String.Empty && elem != headState)
                    listFinals.Add(elem);
            if (listFinals.Count == 1)//если допускающее состояние единственно, оно становится единым конечным состоянием
                unqueFinal = listFinals.First();
            if (state.NonFinalLeft(out stateName) && unqueFinal == String.Empty)//удаляем все недопускающие состояния
                do
                {
                    state.RemoveNonFinalState(stateName);
                } while (state.NonFinalLeft(out stateName));
            if (unqueFinal == String.Empty)//определяем единое конечное состояние, если оно не было определено
                state.TransformGraph(out unqueFinal);
            untouchedStatesList = new List<string> { String.Empty, headState, unqueFinal };//формируем новый список неудаляемых состояний
            state = new StateRemoval(graph, untouchedStatesList, headState, unqueFinal);
            if (state.NonFinalLeft(out stateName))//удаляем все недопускающие состояния
                do
                {
                    state.RemoveNonFinalState(stateName);
                } while (state.NonFinalLeft(out stateName));
            return state.FinalRemoval(graph);//строим регулярное выражения
        }
        /// <summary>
        /// Метод, открывающий стартовое окно приложения.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UsersFiles file = new UsersFiles();
            DialogResult dialogResult = DialogResult.Cancel;
            dialogResult = MessageBox.Show("Are you sure, that you want to start a new project? Current progress will be lost.", "Ensure starting new project", MessageBoxButtons.OKCancel);
            if (dialogResult == DialogResult.OK)
            {
                file.DeleteFiles();
                formChoice.Show();
                Hide();
            }
        }
        /// <summary>
        /// Метод, обрабатывающий закрытие формы. Предупреждает о возможной потери данных.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormUsersFSM_FormClosing(object sender, FormClosingEventArgs e)
        {
            UsersFiles file = new UsersFiles();
            DialogResult result;
            if (e.CloseReason != CloseReason.ApplicationExitCall)
            {
                result = MessageBox.Show("Do you want to quit the application?\nCurrent progress will be lost...", "Ensure quitting the application", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                    e.Cancel = true;
                else
                {
                    file.DeleteFiles();
                    Application.Exit();
                }
            }
        }
        /// <summary>
        /// Метод, открывающий окно с информацией о разработчике.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInfo formInfo = new FormInfo();
            formInfo.ShowDialog();
        }
    }
}
