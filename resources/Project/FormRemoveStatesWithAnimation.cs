﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DeletingAlgo;
using LibGraph;
using System.IO;

namespace Project
{
    /// <summary>
    /// Класс, представляющий окно, являющееся графической реализацией одного шага анимации к методу последовательного удаления состояний.
    /// </summary>
    public partial class FormRemoveStatesWithAnimation : Form
    {
        /// <summary>
        /// Поле - индикатор, было ли добавлено единое конечное состояние. 
        /// </summary>
        bool transformed = false;
        /// <summary>
        /// Поле, хранящее номер открытого шага анимации.
        /// </summary>
        int picShown;
        /// <summary>
        /// Поле, хранящее номер последнего проделанного шага анимации.
        /// </summary>
        int picsMade;
        /// <summary>
        /// Поле, хранящее текущее состояние автомата.
        /// </summary>
        GraphBuilder graph;
        /// <summary>
        /// Поле, хранящее начальное состояние автомата.
        /// </summary>
        string headState;
        /// <summary>
        /// Поле, хранящее путь к изображению с иллюстрацией шага анимации.
        /// </summary>
        string imagePath;
        /// <summary>
        /// Поле, хранящее имя единого конечного состояния.
        /// </summary>
        string unqueFinal=String.Empty;
        /// <summary>
        /// Поле, хранящее номер шага анимации, который будет присвоен.
        /// </summary>
        int count = 1;
        /// <summary>
        /// Поле, представляющее класс с реализацией алгоритма удаления состояний. 
        /// </summary>
        StateRemoval state;
        /// <summary>
        /// Стартовое окно приложения.
        /// </summary>
        ChoiceFSMForm newProjectForm;
        /// <summary>
        /// Поле, хранящее список состояний, которые нельзя удалять (начальное и конечные).
        /// </summary>
        List<string> untouchedStatesList;
        public FormRemoveStatesWithAnimation(GraphBuilder graph, List<string> list, string headState, string imagePath, ChoiceFSMForm form)
        {
            InitializeComponent();
            transformed = false;
            newProjectForm = form;
            buttonPreviousGraph.Enabled = false;
            buttonRemoveNext.Size=buttonRemoveNext.Image.Size;
            buttonPreviousGraph.Size = buttonPreviousGraph.Image.Size;
            buttonSkip.Size = buttonSkip.Image.Size;
            picShown = picsMade = -1;
            List<string> list1 = new List<string>();
            foreach (string elem in list)
                if (elem != String.Empty && elem != headState)
                    list1.Add(elem);
            if(list1.Count==1)
            {
                transformed = true;
                unqueFinal = list1.First();
            }
            untouchedStatesList = list;
            state = new StateRemoval(graph, list, headState,unqueFinal);
            this.graph = graph;
            this.headState = headState;
            pictureBoxGraph.Image = Image.FromStream(new MemoryStream(File.ReadAllBytes(imagePath)));
            pictureBoxGraph.SizeMode = PictureBoxSizeMode.Zoom;
        }
        /// <summary>
        /// Метод, открывающий следующий шаг анимации.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRemoveNext_Click(object sender, EventArgs e)
        {
            buttonPreviousGraph.Enabled = true;
            if (picsMade != picShown)
            {
                if (picShown == -1)
                    picShown = 1;
                else
                picShown++;
                imagePath = String.Format("Graphs" + Path.DirectorySeparatorChar.ToString() + "graphviz{0}.png", picShown);
                pictureBoxGraph.Image = Image.FromStream(new MemoryStream(File.ReadAllBytes(imagePath)));
            }
            else
            {
                string stateName;
                if (state.NonFinalLeft(out stateName) && !transformed)//если остались недопускающие состояния и не было добавлено единое конечное состояние,
                                                                        //запускаем алгоритм удаления недопускающего состояния
                {
                    state.RemoveNonFinalState(stateName);
                    imagePath = graph.MakeDot(count);
                    pictureBoxGraph.Image = Image.FromStream(new MemoryStream(File.ReadAllBytes(imagePath)));
                    pictureBoxGraph.SizeMode = PictureBoxSizeMode.Zoom;
                    if (picsMade == -1)
                        picsMade = picShown = 1;
                    else
                    {
                        picsMade++;
                        picShown++;
                    }
                    count++;
                }
                else if (!transformed)//если остались только допускающие состояния и не было добавлено единое конечное состояние,
                                        //добавляем его
                {
                    state.TransformGraph(out unqueFinal);
                    transformed = true;
                    untouchedStatesList = new List<string> { String.Empty, headState, unqueFinal };
                    state = new StateRemoval(graph, untouchedStatesList, headState, unqueFinal);
                    imagePath = graph.MakeDot(count);
                    pictureBoxGraph.Image = Image.FromStream(new MemoryStream(File.ReadAllBytes(imagePath)));
                    pictureBoxGraph.SizeMode = PictureBoxSizeMode.Zoom;
                    if (picsMade == -1)
                        picsMade = picShown = 1;
                    else
                    {
                        picsMade++;
                        picShown++;
                    }
                    count++;
                }
                else//было добавлено единое конечное состояние
                {
                    if (state.NonFinalLeft(out stateName))//если остались недопускающие состояния, удаляем их
                    {
                        state.RemoveNonFinalState(stateName);
                        imagePath = graph.MakeDot(count);
                        pictureBoxGraph.Image = Image.FromStream(new MemoryStream(File.ReadAllBytes(imagePath)));
                        pictureBoxGraph.SizeMode = PictureBoxSizeMode.Zoom;
                        if (picsMade == -1)
                            picsMade = picShown = 1;
                        else
                        {
                            picsMade++;
                            picShown++;
                        }
                        count++;
                    }
                    else//если осталось только единое конечное состояние и начальное состояние, строим регулярное выражение
                    {
                        List<string> regularExpression;
                        regularExpression = state.FinalRemoval(graph);
                        FormRegularExpression form = new FormRegularExpression(regularExpression, graph,newProjectForm);
                        pictureBoxGraph.SizeMode = PictureBoxSizeMode.Zoom;
                        if (picsMade == -1)
                            picsMade = picShown = 1;
                        else
                        {
                            picsMade++;
                            picShown++;
                        }
                        form.Show();
                        Hide();
                    }
                }
            }
        }
        /// <summary>
        /// Метод, открывающий предыдущий шаг анимации.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPreviousGraph_Click(object sender, EventArgs e)
        {
            if (picShown == 1)
            {
                picShown = -1;
                buttonPreviousGraph.Enabled = false;
            }
            else
                picShown--;
            imagePath = String.Format("Graphs"+Path.DirectorySeparatorChar.ToString()+"graphviz{0}.png", picShown);
            pictureBoxGraph.Image = Image.FromStream(new MemoryStream(File.ReadAllBytes(imagePath)));
        }
        /// <summary>
        /// Метод, открывающий стартовое окно приложения.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UsersFiles file = new UsersFiles();
            DialogResult dialogResult = DialogResult.Cancel;
            dialogResult = MessageBox.Show("Are you sure, that you want to start a new project? Current progress will be lost.", "Ensure starting new project", MessageBoxButtons.OKCancel);
            if (dialogResult == DialogResult.OK)
            {
                file.DeleteFiles();
                newProjectForm.Show();
                Hide();
            }
        }
        /// <summary>
        /// Метод, пропускающий все шаги анимации, начиная с текущего, и открывающий окно с регулярным выражением.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSkip_Click(object sender, EventArgs e)
        {
            FormUsersFSM form = new FormUsersFSM(graph, untouchedStatesList, headState, true, newProjectForm);
            Hide();
        }
        /// <summary>
        /// Метод, обрабатывающий закрытие формы. Предупреждает о возможной потери данных.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormRemoveStatesWithAnimation_FormClosing(object sender, FormClosingEventArgs e)
        {
            UsersFiles file = new UsersFiles();
            DialogResult result;
            if (e.CloseReason != CloseReason.ApplicationExitCall)
            {
                result = MessageBox.Show("Do you want to quit the application?\nCurrent progress will be lost...", "Ensure quitting the application", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                    e.Cancel = true;
                else
                {

                    file.DeleteFiles();
                    Application.Exit();
                }
            }
        }
        /// <summary>
        /// Метод, сохраняющий картинку с иллюстрацией к текущему шагу анимации.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UsersFiles file = new UsersFiles();
            string path = file.ChooseSavingPath("png");
            if (path == string.Empty)
                MessageBox.Show("Error! File was not saved!");
            else
                file.SaveImage(path, (Bitmap)pictureBoxGraph.Image);
        }
        /// <summary>
        /// Метод, открывающий окно с информацией о разработчике.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInfo formInfo = new FormInfo();
            formInfo.ShowDialog();
        }
    }
}
