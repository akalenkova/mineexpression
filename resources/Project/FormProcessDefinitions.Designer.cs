﻿namespace Project
{
    partial class FormProcessDefinitions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormProcessDefinitions));
            this.dataGridViewDefinitions = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.labelAverageLength = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelDic = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDefinitions)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewDefinitions
            // 
            this.dataGridViewDefinitions.AllowUserToAddRows = false;
            this.dataGridViewDefinitions.AllowUserToDeleteRows = false;
            this.dataGridViewDefinitions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDefinitions.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewDefinitions.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridViewDefinitions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.MediumSpringGreen;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewDefinitions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewDefinitions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.MediumSpringGreen;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewDefinitions.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewDefinitions.Location = new System.Drawing.Point(12, 177);
            this.dataGridViewDefinitions.Name = "dataGridViewDefinitions";
            this.dataGridViewDefinitions.ReadOnly = true;
            this.dataGridViewDefinitions.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridViewDefinitions.RowTemplate.Height = 28;
            this.dataGridViewDefinitions.Size = new System.Drawing.Size(1248, 680);
            this.dataGridViewDefinitions.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(623, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(482, 37);
            this.label2.TabIndex = 2;
            this.label2.Text = "Average length of the subprocesses:";
            // 
            // labelAverageLength
            // 
            this.labelAverageLength.AutoSize = true;
            this.labelAverageLength.Font = new System.Drawing.Font("Arial Narrow", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.labelAverageLength.ForeColor = System.Drawing.Color.White;
            this.labelAverageLength.Location = new System.Drawing.Point(996, 78);
            this.labelAverageLength.Name = "labelAverageLength";
            this.labelAverageLength.Size = new System.Drawing.Size(92, 37);
            this.labelAverageLength.TabIndex = 4;
            this.labelAverageLength.Text = "label3";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SteelBlue;
            this.panel1.Controls.Add(this.labelDic);
            this.panel1.Controls.Add(this.labelAverageLength);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1248, 142);
            this.panel1.TabIndex = 5;
            // 
            // labelDic
            // 
            this.labelDic.AutoSize = true;
            this.labelDic.Font = new System.Drawing.Font("Arial Narrow", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.labelDic.ForeColor = System.Drawing.Color.White;
            this.labelDic.Location = new System.Drawing.Point(18, 25);
            this.labelDic.Name = "labelDic";
            this.labelDic.Size = new System.Drawing.Size(162, 37);
            this.labelDic.TabIndex = 5;
            this.labelDic.Text = "Definitions:";
            // 
            // FormProcessDefinitions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1265, 869);
            this.Controls.Add(this.dataGridViewDefinitions);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1287, 925);
            this.Name = "FormProcessDefinitions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expression Miner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormProcessDefinitions_FormClosing);
            this.SizeChanged += new System.EventHandler(this.FormProcessDefinitions_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDefinitions)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewDefinitions;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelAverageLength;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelDic;
    }
}