﻿namespace Project
{
    partial class FormRemoveStatesWithAnimation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRemoveStatesWithAnimation));
            this.buttonRemoveNext = new System.Windows.Forms.Button();
            this.pictureBoxGraph = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.buttonPreviousGraph = new System.Windows.Forms.Button();
            this.panelMain = new System.Windows.Forms.Panel();
            this.buttonSkip = new System.Windows.Forms.Button();
            this.menuStripAnimation = new System.Windows.Forms.MenuStrip();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGraph)).BeginInit();
            this.panelMain.SuspendLayout();
            this.menuStripAnimation.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRemoveNext
            // 
            this.buttonRemoveNext.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonRemoveNext.BackgroundImage")));
            this.buttonRemoveNext.Image = ((System.Drawing.Image)(resources.GetObject("buttonRemoveNext.Image")));
            this.buttonRemoveNext.Location = new System.Drawing.Point(1311, 84);
            this.buttonRemoveNext.Name = "buttonRemoveNext";
            this.buttonRemoveNext.Size = new System.Drawing.Size(65, 64);
            this.buttonRemoveNext.TabIndex = 1;
            this.buttonRemoveNext.UseVisualStyleBackColor = true;
            this.buttonRemoveNext.Click += new System.EventHandler(this.buttonRemoveNext_Click);
            // 
            // pictureBoxGraph
            // 
            this.pictureBoxGraph.Location = new System.Drawing.Point(0, 96);
            this.pictureBoxGraph.Name = "pictureBoxGraph";
            this.pictureBoxGraph.Size = new System.Drawing.Size(2416, 1139);
            this.pictureBoxGraph.TabIndex = 2;
            this.pictureBoxGraph.TabStop = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // buttonPreviousGraph
            // 
            this.buttonPreviousGraph.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonPreviousGraph.BackgroundImage")));
            this.buttonPreviousGraph.Image = ((System.Drawing.Image)(resources.GetObject("buttonPreviousGraph.Image")));
            this.buttonPreviousGraph.Location = new System.Drawing.Point(1112, 84);
            this.buttonPreviousGraph.Name = "buttonPreviousGraph";
            this.buttonPreviousGraph.Size = new System.Drawing.Size(65, 64);
            this.buttonPreviousGraph.TabIndex = 3;
            this.buttonPreviousGraph.UseVisualStyleBackColor = true;
            this.buttonPreviousGraph.Click += new System.EventHandler(this.buttonPreviousGraph_Click);
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.SteelBlue;
            this.panelMain.Controls.Add(this.buttonSkip);
            this.panelMain.Controls.Add(this.buttonPreviousGraph);
            this.panelMain.Controls.Add(this.buttonRemoveNext);
            this.panelMain.Location = new System.Drawing.Point(-21, 1304);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(2595, 216);
            this.panelMain.TabIndex = 4;
            // 
            // buttonSkip
            // 
            this.buttonSkip.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSkip.BackgroundImage")));
            this.buttonSkip.Image = ((System.Drawing.Image)(resources.GetObject("buttonSkip.Image")));
            this.buttonSkip.Location = new System.Drawing.Point(1596, 84);
            this.buttonSkip.Name = "buttonSkip";
            this.buttonSkip.Size = new System.Drawing.Size(65, 64);
            this.buttonSkip.TabIndex = 4;
            this.buttonSkip.UseVisualStyleBackColor = true;
            this.buttonSkip.Click += new System.EventHandler(this.buttonSkip_Click);
            // 
            // menuStripAnimation
            // 
            this.menuStripAnimation.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStripAnimation.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.menuStripAnimation.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStripAnimation.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.newProjectToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStripAnimation.Location = new System.Drawing.Point(0, 0);
            this.menuStripAnimation.Name = "menuStripAnimation";
            this.menuStripAnimation.Size = new System.Drawing.Size(2564, 40);
            this.menuStripAnimation.TabIndex = 5;
            this.menuStripAnimation.Text = "menuStrip1";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(155, 36);
            this.saveToolStripMenuItem.Text = "New Project";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.newProjectToolStripMenuItem_Click);
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(77, 36);
            this.newProjectToolStripMenuItem.Text = "Save";
            this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(92, 36);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // FormRemoveStatesWithAnimation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(2564, 1555);
            this.Controls.Add(this.menuStripAnimation);
            this.Controls.Add(this.pictureBoxGraph);
            this.Controls.Add(this.panelMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripAnimation;
            this.Name = "FormRemoveStatesWithAnimation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expression Miner";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRemoveStatesWithAnimation_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGraph)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.menuStripAnimation.ResumeLayout(false);
            this.menuStripAnimation.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonRemoveNext;
        private System.Windows.Forms.PictureBox pictureBoxGraph;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.Button buttonPreviousGraph;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.MenuStrip menuStripAnimation;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.Button buttonSkip;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}