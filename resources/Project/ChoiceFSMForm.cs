﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Project
{
    /// <summary>
    /// Класс, представляющий стартовое окно приложения с возможностью выбрать, каким образом начать работу с программой.
    /// </summary>
    public partial class ChoiceFSMForm : Form
    {
        public ChoiceFSMForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Метод, обрабатывающий желание пользователя создать конечный автомат и открывающий окно отрисовки автомата.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCreate_Click(object sender, EventArgs e)
        {
            Draw draw = new Draw(this);
            Directory.CreateDirectory(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar.ToString() + "Graphs");
            draw.Show();
            Hide();

        }
        /// <summary>
        /// Метод, открывающий окно с информацией о разработчике.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInfo formInfo = new FormInfo();
            formInfo.ShowDialog();
        }
        /// <summary>
        /// Метод, обрабатывающий желание пользователя загрузить автомат из стороннего ресурса и открывающий окно открытия файла.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFromFile_Click(object sender, EventArgs e)
        {
            UsersFiles file = new UsersFiles();
            bool opened = file.OpenFile();
            Directory.CreateDirectory(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar.ToString() + "Graphs");
            TSMLtransformer tSML = new TSMLtransformer(file.FilePath,this, opened);
        }
        /// <summary>
        /// Метод, завершающий работу приложения при закрытии окна.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChoiceFSMForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void dictionaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            File.Open("dictionary.txt", FileMode.Open);
        }
    }
}