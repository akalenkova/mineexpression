﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LibGraph;

namespace Project
{
    /// <summary>
    /// Класс, представляющий окно, содержащее регулярное выражение, построенное по пользовательскому конечному автомату, и предоставляющее доступ к аналитическим данным.
    /// </summary>
    public partial class FormRegularExpression : Form
    {
        /// <summary>
        /// Конечный автомат, по которому было построено регулярное выражение.
        /// </summary>
        GraphBuilder graph;
        /// <summary>
        /// Окно, содержащее определение процессов.
        /// </summary>
        FormProcessDefinitions formBasicsDictionary;
        /// <summary>
        /// Окно, содержащее определение подпроцессов.
        /// </summary>
        FormProcessDefinitions formFinalsDictionary;
        /// <summary>
        /// Окно, содержащее регуляное выражение, полученное по конечному автомату без использования алгоритма минимизации регулярного выражения.
        /// </summary>
        FormBefore formBefore;
        /// <summary>
        /// Окно, позволяющее пользователю задать параметры сохранения.
        /// </summary>
        FormSavingRules formSavingRules;
        /// <summary>
        /// Строки, хранящие регулярное выражение.
        /// </summary>
        List<string> regularExpression;
        /// <summary>
        /// Стартовое оено приложения.
        /// </summary>
        ChoiceFSMForm newProjectForm;
        /// <summary>
        /// Свойство, считывающее содержимое richTextBoxRegularExpression. (Регулярное выражение).
        /// </summary>
        List<string> textBoxContents
        {
            get
            {
                List<string> contents = new List<string>();
                foreach (string line in richTextBoxRegularExpression.Lines)
                    contents.Add(line);
                return contents;
            }
        }

        public FormRegularExpression(List<string> regularExpression, GraphBuilder graph, ChoiceFSMForm form)
        {
            InitializeComponent();
            newProjectForm = form;
            Height = richTextBoxRegularExpression.Location.Y + richTextBoxRegularExpression.Height + 80;
            this.regularExpression = regularExpression;
            this.graph = graph;
            richTextBoxRegularExpression.Text = String.Empty;
            foreach (string expression in regularExpression)
                richTextBoxRegularExpression.Text+= expression;
            char[] charArr = richTextBoxRegularExpression.Text.ToCharArray();
            int count = 0;
            foreach (char c in charArr)
            {
                if (c != '+' && c != '.' && c != ')' && c != '(' && c != '*' && !(c >= 1 && c <= 9))
                    count++;
            }
            labelNoDic.Text = count.ToString();
            formBasicsDictionary = new FormProcessDefinitions(graph.basicDefinitions, graph.finalDefinitions);
            formFinalsDictionary = new FormProcessDefinitions(graph.basicDefinitions);
            formBefore = new FormBefore(regularExpression, graph.finalDefinitions);
            formSavingRules = new FormSavingRules(this);
        }
        /// <summary>
        /// Метод, открывающий окно с определениями подпроцессов.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDefinitions_Click(object sender, EventArgs e)
        {
            formBasicsDictionary.Visible=true;
        }
        /// <summary>
        /// Метод, открывающий окно с определениями процессов.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBasics_Click(object sender, EventArgs e)
        {
            formFinalsDictionary.Visible=true;
        }
        /// <summary>
        /// Метод, открывающий окно с регуляным выражением, полученным по конечному автомату без использования алгоритма минимизации регулярного выражения.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBefore_Click(object sender, EventArgs e)
        {
            formBefore.Visible=true;
        }
        /// <summary>
        /// Метод, обрабатывающий закрытие формы. Предупреждает о возможной потери данных.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormRegularExpression_FormClosing(object sender, FormClosingEventArgs e)
        {
            UsersFiles file = new UsersFiles();
            DialogResult result;
            if (e.CloseReason != CloseReason.ApplicationExitCall)
            {
                result = MessageBox.Show("Do you want to quit the application?\nCurrent progress will be lost...", "Ensure quitting the application", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                    e.Cancel = true;
                else
                {
                    file.DeleteFiles();
                    Application.Exit();
                }
            }
        }
        /// <summary>
        /// Метод, открывающий окно с информацией о разработчике.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInfo formInfo = new FormInfo();
            formInfo.ShowDialog();
        }
        /// <summary>
        /// Метод, открывающий стартовое окно приложения.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UsersFiles file = new UsersFiles();
            DialogResult dialogResult = DialogResult.Cancel;
            dialogResult = MessageBox.Show("Are you sure, that you want to start a new project? Current progress will be lost.", "Ensure starting new project", MessageBoxButtons.OKCancel);
            if (dialogResult == DialogResult.OK)
            {
                file.DeleteFiles();
                newProjectForm.Show();
                Hide();
            }
        }
        /// <summary>
        /// Метод, обрабатывающий желание пользователя сохранить данные, полученные в результате работы алгоритма, и запускающий выбор параметров сохранения.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formSavingRules = new FormSavingRules(this);
            formSavingRules.Show();
        }
        /// <summary>
        /// Метод, завершающий сохранение данных в зависимости от выбранных пользователем параметров сохранения.
        /// </summary>
        public void CompleteSaving()
        {
            if (formSavingRules.SavingCompleted)
            {
                List<string> textAfter=new List<string>();
                List<string> textBefore = new List<string>();
                List<string> textBasics = new List<string>();
                List<string> textProcessDef = new List<string>();
                List<string> textAnalytics = new List<string>();
                if (formSavingRules.AfterAlgo)
                {
                    textAfter.Add("Expression mined after applying the algorithm: ");
                    foreach (string line in textBoxContents)
                        textAfter.Add(line);
                    textAfter.Add(Environment.NewLine);
                }
                if(formSavingRules.BeforeAlgo)
                {
                    textBefore.Add("Expression mined without applying the algorithm: ");
                    foreach (string line in formBefore.textBoxContents)
                        textBefore.Add(line);
                    textBefore.Add(Environment.NewLine );
                }
                if(formSavingRules.BasicDefinitions)
                {
                    textBasics.Add("Subprocess definitions: ");
                    foreach (string line in formBasicsDictionary.tableContents)
                        textBasics.Add(line);
                    textBasics.Add(Environment.NewLine);
                }
                if (formSavingRules.ProcessDefinitions)
                {
                    textProcessDef.Add("Process definitions: "+Environment.NewLine);
                    foreach (string line in formFinalsDictionary.tableContents)
                        textProcessDef.Add(line);
                    textProcessDef.Add(Environment.NewLine );
                }
                if(formSavingRules.Analytics)
                {
                    textAnalytics.Add("Analysis of the effectiveness of the applied algorithm:"+Environment.NewLine);
                    textAnalytics.Add($"Number of events after applying the algorithm: " + labelNoDic.Text+".");
                    textAnalytics.Add($"Number of events before applying the algorithm: "+formBefore.BeforeCount + ".");
                    textAnalytics.Add("Maximum nesting: " + formBefore.MaxNesting + ".");
                    textAnalytics.Add("Average length of the subprocesses: " + formBasicsDictionary.MidLength + ".");
                }
                UsersFiles file = new UsersFiles();
                file.SaveMinedExpression(textAfter, textBefore, textBasics, textProcessDef, textAnalytics);
            }
            else
                MessageBox.Show("Error! You have not chosen information to save...");
        }
    }
}
