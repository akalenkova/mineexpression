﻿using System;
using System.Windows.Forms;

namespace Project
{
    /// <summary>
    /// Класс, представляющий окно, позволяющее пользователю задать параметры сохранения.
    /// </summary>
    public partial class FormSavingRules : Form
    {
        /// <summary>
        /// Поле - индикатор, желает ли пользователь сохранить регулярное выражение, полученное с применением алгоритма минимизации.
        /// </summary>
        bool afterAlgo;
        /// <summary>
        /// Поле - индикатор, желает ли пользователь сохранить регулярное выражение, полученное без применения алгоритма минимизации.
        /// </summary>
        bool beforeAlgo;
        /// <summary>
        /// Поле - индикатор, желает ли пользователь сохранить определения процессов.
        /// </summary>
        bool basicDefinitions;
        /// <summary>
        /// Поле - индикатор, желает ли пользователь сохранить определения подпроцессов.
        /// </summary>
        bool processDefinitions;
        /// <summary>
        /// Поле - индикатор, желает ли пользователь сохранить аналитические данные.
        /// </summary>
        bool analytics;
        /// <summary>
        /// Поле - индикатор, было ли завершено сохранение.
        /// </summary>
        bool savingCompleted;
        /// <summary>
        /// Свойство, делающее поле afterAlgo доступным для чтения.
        /// </summary>
        public bool AfterAlgo { get { return afterAlgo; } }
        /// <summary>
        /// Свойство, делающее поле beforeAlgo доступным для чтения.
        /// </summary>
        public bool BeforeAlgo { get { return beforeAlgo; } }
        /// <summary>
        /// Свойство, делающее поле basicDefinitions доступным для чтения.
        /// </summary>
        public bool BasicDefinitions { get { return basicDefinitions; } }
        /// <summary>
        /// Свойство, делающее поле processDefinitions доступным для чтения.
        /// </summary>
        public bool ProcessDefinitions { get { return processDefinitions; } }
        /// <summary>
        /// Свойство, делающее поле analytics доступным для чтения.
        /// </summary>
        public bool Analytics { get { return analytics; } }
        /// <summary>
        /// Свойство, делающее поле savingCompleted доступным для чтения. 
        /// </summary>
        public bool SavingCompleted { get { return savingCompleted; } }
        /// <summary>
        /// Окно, содержащее регулярное выражение, полученное из конечного автомата.
        /// </summary>
        FormRegularExpression formRE;

        public FormSavingRules(FormRegularExpression form)
        {
            InitializeComponent();
            formRE = form;
        }
        /// <summary>
        /// Метод, вызываемый при желании пользователя сохранить некоторые из доступных аналитических данных.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSaveChosen_Click(object sender, EventArgs e)
        {
            MarkChecked();
        }
        /// <summary>
        /// Метод, вызываемый при желании пользователя сохранить абсолютно все аналитические данные.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddAll_Click(object sender, EventArgs e)
        {
            checkBoxAfterAlgo.Checked = true;
            checkBoxAnalytics.Checked = true;
            checkBoxBasics.Checked = true;
            checkBoxBeforeAlgo.Checked = true;
            checkBoxProcessDefinitions.Checked = true;
            MarkChecked();
        }
        /// <summary>
        /// Метод, задающий параметры сохранения в зависимости от выбранных пользователем характеристик, и запускающий процесс сохранения.
        /// </summary>
        void MarkChecked()
        {
             afterAlgo= checkBoxAfterAlgo.Checked;
             beforeAlgo= checkBoxBeforeAlgo.Checked;
             basicDefinitions= checkBoxBasics.Checked;
             processDefinitions= checkBoxProcessDefinitions.Checked;
             analytics= checkBoxAnalytics.Checked;
            if (afterAlgo || beforeAlgo || basicDefinitions || processDefinitions || analytics)
                savingCompleted = true;
            formRE.CompleteSaving();
            Hide();
        }
    }
}
