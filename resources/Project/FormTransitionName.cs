﻿using System;
using System.Windows.Forms;

namespace Project
{
    /// <summary>
    /// Класс, позволяющий ввести регулярное выражение, задающее переход.
    /// </summary>
    public partial class FormTransitionName : Form
    {
        /// <summary>
        /// Окно отрисовки.
        /// </summary>
        Draw form;
        /// <summary>
        /// Введенное регулярное выражение, задающее переход.
        /// </summary>
        string transition;
        public FormTransitionName(Draw form)
        {
            InitializeComponent();
            this.form = form;
            textBoxMain.Select();
            transition = "";
        }
        
        /// <summary>
        /// Добавляет переход перед закрытием формы.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormTransitionName_FormClosed(object sender, FormClosedEventArgs e)
        {
            form.CompleteAddingEdge(transition);//добавляем переход или даем сигнал, что пользователь не завершил добавление перехода
        }
        /// <summary>
        /// Проверяет корректность введенного перехода.
        /// </summary>
        /// <param name="input">ввод пользователя</param>
        /// <returns></returns>
        bool CheckInput(string input)
        {
            char[] arr = input.ToCharArray();
            foreach (char c in arr)
                if (!(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= 'а' && c <= 'я' || c >= 'А' || c <= 'Я'))
                    return false;
            return true;
        }
        /// <summary>
        /// Обрабатывает запрос пользователя на добавление перехода.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (textBoxMain.Text == "")
                MessageBox.Show("Error! Please, enter transition...");
            else if (CheckInput(textBoxMain.Text))
            {
                transition = textBoxMain.Text;
                Close();
            }
            else
                MessageBox.Show("Error! Invalid transition value...");
        }
    }
}
