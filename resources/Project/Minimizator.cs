﻿using System;
using System.Collections.Generic;
using System.Linq;
using LibGraph;
using System.Windows.Forms;

namespace Project
{
    /// <summary>
    /// Класс, хранящий символ перехода и состояние назначения.
    /// </summary>
    class OutRange
    {
        /// <summary>
        /// Поле, хранящее символ перехода.
        /// </summary>
        string transition;
        /// <summary>
        /// Поле, хранящее состояние в которое осуществляется переход.
        /// </summary>
        string distinationState;
        /// <summary>
        /// Свойство, делающее поле transition доступным для чтения.
        /// </summary>
        public string Transition { get { return transition; } }
        /// <summary>
        /// Свойство, делающее поле distinationState доступным для чтения.
        /// </summary>
        public string DictinationState { get { return distinationState; } }
        public OutRange(string symb, string state)
        {
            transition = symb;
            distinationState = state;
        }
    }
    /// <summary>
    /// Класс, представляющий сплиттер и хранящий символ разбиения и номер класса эквивалентности.
    /// </summary>
    class Splitter
    {
        /// <summary>
        /// Поле, хранящее символ, по которому идет разбиение.
        /// </summary>
        string symb;
        /// <summary>
        /// Поле, хранящее номер класса эквивалентности. 
        /// </summary>
        int equivalenceClass;
        /// <summary>
        /// Свойство, делающее поле symb доступным для чтения.
        /// </summary>
        public string Symbol
        {
            get { return symb; }
        }
        /// <summary>
        /// Свойство, делающее поле equivalenceClass доступным для чтения.
        /// </summary>
        public int EquivalenceClass
        {
            get { return equivalenceClass; }
        }
        public Splitter(string symbol, int classNum)
        {
            symb = symbol;
            equivalenceClass = classNum;
        }
    }
    /// <summary>
    /// Класс, выполняющий минимизацию конечного автомата.
    /// </summary>
    class Minimizator
    {
        /// <summary>
        /// Поле, хранящее словарь исходящих дуг, ключом является состояние, значением - список дуг.
        /// </summary>
        Dictionary<string, List<OutRange>> StateInfoDictionary = new Dictionary<string, List<OutRange>>();
        /// <summary>
        /// Поле, хранящее словарь классов эквивалентности, ключом является класс, значением - список состояний.
        /// </summary>
        Dictionary<int, List<string>> classMembers = new Dictionary<int, List<string>>();
        /// <summary>
        /// Поле, хранящее список всех возможных переходов (алфавит).
        /// </summary>
        List<string> possibleTransitions = new List<string>();
        const string trap = "trap";
        /// <summary>
        /// Поле, представляющее очередь из пар типа (символ перехода) - (класс).
        /// </summary>
        Queue<Splitter> queue = new Queue<Splitter>();
        /// <summary>
        /// Поле, хранящее первоначальный список допускающих состояний.
        /// </summary>
        List<string> acceptStates;
        /// <summary>
        /// Поле, хранящее начальное состояние.
        /// </summary>
        string startingState;
        /// <summary>
        /// Свойство доступа к полю startingState.
        /// </summary>
        public string StartingState { get; private set; }
        public Minimizator(List<string> finalStates, string startingState, GraphBuilder graph)
        {
            this.startingState = startingState;
            acceptStates = finalStates;
            foreach (var edge in graph.myGraph.Edges)
                if (!possibleTransitions.Contains(edge.Tag)&&edge.Tag!=String.Empty)
                    possibleTransitions.Add(edge.Tag);
            List<OutRange> list = new List<OutRange>();
            foreach (string transition in possibleTransitions)//добавляем в ловушку переходы по всем символам из нее самой
                list.Add(new OutRange(transition, trap));
            StateInfoDictionary.Add(trap, list);//добавляем информацию о ловушке
            classMembers.Add(1, new List<string>());//инициализируем два начальных класса эквивалентности
            classMembers.Add(2, new List<string>());
            classMembers[1].Add(trap);
            foreach (string node in graph.myGraph.Vertices)//заполняем информацию об исходящих дугах для каждого состояния
                if(node!=String.Empty)
                {
                    list = new List<OutRange>();//список дуг
                    List<string> usedTransitions = new List<string>();//использованные символы
                    foreach(var edge in graph.myGraph.OutEdges(node))
                    {
                        list.Add(new OutRange(edge.Tag, edge.Target));
                        if (!usedTransitions.Contains(edge.Tag))
                            usedTransitions.Add(edge.Tag);
                    }
                    foreach (string transition in possibleTransitions)//по каждому неиспользованному символу добавляем переход в ловушку
                        if (!usedTransitions.Contains(transition))
                            list.Add(new OutRange(transition, trap));
                    if (finalStates.Contains(node))
                    {
                        StateInfoDictionary.Add(node, list);//доступ по состоянию к списку дуг
                        classMembers[2].Add(node);//если состояние конечное, принадлежит ко 2 классу
                    }
                    else
                    {
                        StateInfoDictionary.Add(node, list);
                        classMembers[1].Add(node);//если состояние конечное, принадлежит к 1 классу
                    }
                }
            foreach (string symbol in possibleTransitions)//добавляем в очередь все пары (символ перехода, класс)
            {
                queue.Enqueue(new Splitter(symbol, 1));
                queue.Enqueue(new Splitter(symbol, 2));
            }
        }
        /// <summary>
        /// Метод, который находит минимальное свободное число для номера класса разбиения.
        /// </summary>
        /// <returns>имя класс</returns>
        int FindFreeClassName()
        {
            int count = 0;
            foreach (var member in queue)
                if (member.EquivalenceClass > count)
                    count = member.EquivalenceClass;
            return count+1;
        }
        /// <summary>
        /// Метод, разбивающий один класс эквивалентности.
        /// </summary>
        /// <param name="key">Номер азбиваемого класса.</param>
        /// <param name="symbol">Символ разбиения.</param>
        /// <param name="equivalenceClass">Сплиттер.</param>
        bool Split(int key, string symbol, int equivalenceClass)
        {
            int createdClass = 0;//номер созданного класса
            bool broken = false;//разбилось ли множество на подмножества по символу
            List<string> r1_inSplitter;//множество состояний, переходящих по данному символу в сплиттер
            List<string> r2_Others;//множество состояний,не переходящих по данному символу в сплиттер
            bool goesToSplitter;//переходит ли данное состояние по данному символу в сплиттер
            r1_inSplitter = new List<string>();
            r2_Others = new List<string>();
            foreach (string member in classMembers[key])//для каждого состояния из данного класса
            {
                goesToSplitter = false;
                foreach (OutRange edge in StateInfoDictionary[member])//если дуга по символу из данного состояния переходит в сплиттер, добавляем состояние в один класс, иначе - в другой
                    if (edge.Transition == symbol)
                    {
                        foreach (string node in classMembers[equivalenceClass])
                            if (edge.DictinationState == node)
                            {
                                goesToSplitter = true;
                                break;
                            }
                        if (goesToSplitter)
                            r1_inSplitter.Add(member);
                        else
                            r2_Others.Add(member);
                    }
            }
            if (r1_inSplitter.Count != 0 && r2_Others.Count != 0)//если множество разбилось на подмножества, добавляем новый класс, изменяем текущий
            {
                broken = true;
                int num = classMembers[key].Count;
                for (int index = 0; index < num; index++)//удаляем из текущего класса состояния, не переходящие в сплиттер
                {
                    foreach (string state in classMembers[key])
                        if (r2_Others.Contains(state))
                        {
                            classMembers[key].Remove(state);
                            break;
                        }
                }
                classMembers.Add(createdClass = FindFreeClassName(), r2_Others);//добавляем новый класс с состояниями, не переходящими в сплиттер
                List<string> lostTransitions = new List<string>();
                foreach (string transition in possibleTransitions)//какими переходами нужно дополнить разбитый класс
                    lostTransitions.Add(transition);
                foreach(var element in queue)
                    if (element.EquivalenceClass == key)
                        lostTransitions.Remove(element.Symbol);
                    foreach (string symb in possibleTransitions)
                        queue.Enqueue(new Splitter(symb, createdClass));//добавляем в очередь пары из всех возможных символов и нового класса
                foreach (string symb in possibleTransitions)
                    if (lostTransitions.Contains(symb))//дополняем очередь парами так, чтобы были пары из всех символов и измененного класса
                    queue.Enqueue(new Splitter(symb, key));
            }
            return broken;
        }
        /// <summary>
        /// Метод, реализующий алгоритм разбиения множества состояний на минимальные подмножества.
        /// </summary>
        void Divide()
        {
            string symbol;//символ перехода
            int equivalenceClass;//номер класса эквивалентности
            while (queue.Count!=0)//пока в очереди остались пары символ-класс, делим классы
            {
                symbol = queue.First().Symbol;//первый в очереди символ
                equivalenceClass = queue.First().EquivalenceClass;//первый в очереди класс
                int numOfClasses = classMembers.Keys.Count;//количество классов на данный момент
                for (int key = 1; key < numOfClasses+1; key++)//обход всех классов, кроме сплиттера
                {
                        if (key!=equivalenceClass)//если класс не сплиттер
                        Split(key, symbol, equivalenceClass);
                }
                if(!Split(equivalenceClass,symbol,equivalenceClass))//разбиваем сам сплиттер
                queue.Dequeue();//отработавшая пара сплиттер-символ покидает очередь, если сплиттер не был разбит на подмножества
            }
        }
        /// <summary>
        /// Метод, реализующий построение минимизированного автомата.
        /// </summary>
        /// <returns>автомат с минимальным числом состояний</returns>
        public GraphBuilder Minimize()
        {
            Divide();//минимальное разбиение
            GraphBuilder graph = new GraphBuilder();//новый граф
            int trapClass=0;
            foreach (int key in classMembers.Keys)//для каждого класса
                if (classMembers[key].Contains(trap))
                    trapClass = key;
            foreach (int key in classMembers.Keys)//для каждого класса
                if(key!=trapClass)//если в этом классе нет ловушки
                {
                    try
                    {
                        if (acceptStates.Contains(classMembers[key].First()))//если в классе допускающие состояния, делаем новое состояние допускающим
                            graph.UserAddFinalVertix(key.ToString());
                    
                    else
                        graph.UserAddVertix(key.ToString());//иначе обычное состояние
                    }
                    catch (InvalidOperationException)
                    {
                        MessageBox.Show("Automata without final states cannot be minimized...", "Error!", MessageBoxButtons.OK);
                        return null;
                    }
                    if (classMembers[key].Contains(startingState))//если в множестве есть начальное состояние, делаем новое состояние начальным
                    {
                        StartingState = key.ToString();
                        graph.UserAddVertix(String.Empty);
                        graph.UserAddEdge(String.Empty, key.ToString(), String.Empty);
                    }
                }
            foreach (int key in classMembers.Keys)//для каждого класса
                if (key != trapClass)//если в этом классе нет ловушки
                {
                    string state = classMembers[key].First();//смотрим на первое состояние в классе, потому что все состояния эквивалентны
                    foreach (var edge in StateInfoDictionary[state])//обход всех исходящих из данного состояния дуг
                    {
                        string distinationState="";
                        if(!classMembers[trapClass].Contains(edge.DictinationState))//если дуга не в ловушку
                        {
                            foreach(int className in classMembers.Keys)//ищем, в каком классе лежит состояние, куда переходит дуга
                                if(classMembers[className].Contains(edge.DictinationState))
                                {
                                    distinationState = className.ToString();
                                    break;
                                }
                            graph.UserAddEdge(key.ToString(), distinationState, edge.Transition);
                        }
                    }
                }
           return graph;
        }
    }
}
