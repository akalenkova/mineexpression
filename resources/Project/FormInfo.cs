﻿using System.Windows.Forms;

namespace Project
{
    /// <summary>
    /// Класс, представляющий окно, содержающее информацию о разработчике.
    /// </summary>
    public partial class FormInfo : Form
    {
        public FormInfo()
        {
            InitializeComponent();
        }
    }
}
