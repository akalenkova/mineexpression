﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Project
{
    /// <summary>
    /// Класс, представляющий окно, содержащее регуляное выражение, полученное по конечному автомату без использования алгоритма минимизации регулярного выражения.
    /// Содержит аналитические данные: длину выражения до минимизации и максимальную вложенность.
    /// </summary>
    public partial class FormBefore : Form
    {
        /// <summary>
        /// Длина регулярного выражения до минимизации.
        /// </summary>
        public string BeforeCount { get { return labelNumOfEvents.Text; } }
        /// <summary>
        /// Максимальная вложенность.
        /// </summary>
        public string MaxNesting { get { return labelMaxNesting.Text; } }
        /// <summary>
        /// Содержимое richTextBoxBefore (регулярное выражение).
        /// </summary>
        public List<string> textBoxContents
        {
            get
            {
                List<string> contents = new List<string>();
                foreach (string line in richTextBoxBefore.Lines)
                    contents.Add(line);
                return contents;
            }
        }
        public FormBefore(List<string> regularExpressions, Dictionary<string,string> finals)
        {
            InitializeComponent();
            Height = richTextBoxBefore.Location.Y + richTextBoxBefore.Height + 80;
            Dictionary<string, string> contents = new Dictionary<string, string>();
            List<string> expressions = new List<string>();
            int count = 0;
            //заполняем richTextBoxBefore
            bool found = false;
            do
            {
                foreach (string key in finals.Keys)
                {
                    found = false;
                    string str;
                    if (!contents.Keys.Contains(key))
                        str = Change(finals[key]);
                    else
                        str = Change(contents[key]);
                    char[] array = str.ToCharArray();
                    str = "";
                    string s = "";
                    for(int i=0;i<array.Length;i++)
                    {
                        if (!(array[i] >= 'A' && array[i] <= 'Z' || array[i] >= 'a' && array[i] <= 'z' || array[i] >= 0 && array[i] <= 9))
                        {
                            if (s != "")
                            {
                                if (finals.Keys.Contains(s))
                                {
                                    if (str.Length != 0 && str[str.Length - 1] == '.')
                                        str += "(" + Change(finals[s]) + ")";
                                    else
                                        str += Change(finals[s]);
                                    found = true;
                                }
                                else
                                    str += s;
                            }
                            s = "";
                            str += array[i];
                        }
                        else
                            s += array[i];
                    }
                    if (s != "")
                        {
                        if (finals.Keys.Contains(s))
                        {
                            if (str.Length != 0 && str[str.Length - 1] == '.')
                                str += "(" + Change(finals[s]) + ")";
                            else
                                str += Change(finals[s]);
                            found = true;
                        }
                        else
                            str += s;
                        }
                    if (!contents.Keys.Contains(key))
                        contents.Add(key, str);
                    else
                        contents[key] = str;
                }
                if (found)
                    count++;
            } while (found);
            labelMaxNesting.Text = count.ToString();
            foreach(string str in regularExpressions)
            {
                string result = "";
                char[] charArr = str.ToCharArray();
                string s = "";
                for (int i = 0; i < charArr.Length; i++)
                {
                    if (!(charArr[i] >= 'A' && charArr[i] <= 'Z' || charArr[i] >= 'a' && charArr[i] <= 'z' || charArr[i] >= 0 && charArr[i] <= 9))
                    {
                        if (s != "")
                        {
                            if (contents.Keys.Contains(s))
                            {
                                result += contents[s];
                                found = true;
                            }
                            else
                                result += s;
                        }
                        s = "";
                        result += charArr[i];
                    }
                    else
                        s += charArr[i];
                }
                if (s != "")
                {
                    if (contents.Keys.Contains(s))
                    {
                        result += finals[s];
                        found = true;
                    }
                    else
                        result += s;
                }
                expressions.Add(result);
            }
            count = 0;
            foreach (string expression in expressions)
            {
                richTextBoxBefore.Text += expression;
                char[]charArr = expression.ToCharArray();
                foreach (char c in charArr)
                {
                    if (c != '+' && c != '.' && c != ')' && c != '(' && c != '*'&&!(c>=1&&c<=9))
                    count++;
                }
            }
            labelNumOfEvents.Text = count.ToString();
        }
        /// <summary>
        /// Изменение регулярного выражения. (Удаление символов, добавленных вместе с единым конечным состоянием).
        /// </summary>
        /// <param name="expression">Регулярное выражение.</param>
        /// <returns></returns>
        string Change(string expression)
        {
            string result = "";
            char[] expArr = expression.ToCharArray();
            for (int i = 0; i < expArr.Length; i++)
            {
                if (expArr[i] == '&' && i != 0 && result[result.Length - 1] == '.')
                    result = result.Remove(result.Length - 1);
                else if (expArr[i] == '+' && i != 0 && result[result.Length - 1] == '&')
                {
                    result = result.Remove(result.Length - 1);
                    result += (char)400;
                    result += expArr[i];
                }
                else
                    result += expArr[i];
            }
            return result;
        }
        /// <summary>
        /// Обработка закрытие формы. (Объект формы не должен удаляться).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormBefore_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.ApplicationExitCall)
            {
                e.Cancel = true;
                Visible = false;
            }
        }
    }
}
