﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using LibGraph;
using DrawingSupporter;

namespace Project
{
    /// <summary>
    /// Класс, задающий вершину графа, как ее имя и идентификационное имя (необходимо для сохранения в формате .tsml).
    /// </summary>
    class TSMlVertex
    {
        /// <summary>
        /// Имя вершины.
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Идентификационное имя вершины.
        /// </summary>
        public string id { get; set; }
    }
    /// <summary>
    /// Класс, осуществляющий преобразование конечного автомата, заданного в формате .tsml в формат, используемый программой, и наоборот.
    /// </summary>
    class TSMLtransformer
    {
        /// <summary>
        /// Словарь состояний. Ключом является имя состояния, значением - ее идентификационное имя.
        /// </summary>
        Dictionary<string, string> stateID;
        /// <summary>
        /// Начальное состояние автомата.
        /// </summary>
        string startingState;
        /// <summary>
        /// Список конечных состояний автомата.
        /// </summary>
        List<string> finalStates;
        /// <summary>
        /// Документ в формате .tsml.
        /// </summary>
        XmlDocument doc;
        /// <summary>
        /// Конечный автомат.
        /// </summary>
        GraphBuilder graph;
        /// <summary>
        /// Поле для записи информации в формате .xml.
        /// </summary>
        XmlTextWriter textWritter;
        /// <summary>
        /// Конструктор, вызываемый при необходимости преобразовать конечный автомат, заданный в формате .tsml в формат, используемый программой.
        /// </summary>
        /// <param name="filePath">Путь к файлу в формате .tsml.</param>
        /// <param name="formChoice">Стартовое окно приложения.</param>
        /// <param name="opened">Индикатор, был ли открыт файл.</param>
        public TSMLtransformer(string filePath,ChoiceFSMForm formChoice, bool opened)
        {
            if (opened)
            {
                formChoice.Hide();
                graph = new GraphBuilder();
                doc = new XmlDocument();
                doc.Load(filePath);
                XmlElement root = doc.DocumentElement;
                GetEdgeList(root, GetNodeList(root));
                FormUsersFSM form = new FormUsersFSM(graph, finalStates, startingState,formChoice);
                if (form.Success == true)
                    form.Show();
                else
                    formChoice.Show();
            }
        }
        /// <summary>
        /// Конструктор, вызываемый при необходимости преобразовать конечный автомат, заданный в формате формат, используемым программой в формат .tsml.
        /// </summary>
        /// <param name="path">Путь к созданному файлу.</param>
        /// <param name="stateList">Список состояний.</param>
        /// <param name="finalStateList">Список конечных состояний.</param>
        /// <param name="startingState">Начальное состояние.</param>
        /// <param name="edgeList">Список переходов.</param>
        public TSMLtransformer(string path, List<Vertex> stateList,List<string> finalStateList, string startingState, List<Edge> edgeList)
        {
            stateID = new Dictionary<string, string>();
            doc = new XmlDocument();
            CreateTSML(path,stateList, finalStateList, startingState, edgeList);
        }
        /// <summary>
        /// Метод, выполняющий сохранение конечного автомата в формате .tsml.
        /// </summary>
        /// <param name="path">Путь к сохраняемому файлу.</param>
        /// <param name="stateList">Список состояний.</param>
        /// <param name="finalStateList">Список конечных состояний.</param>
        /// <param name="startingState">Начальное состояние.</param>
        /// <param name="edgeList">Список переходов.</param>
        void CreateTSML(string path, List<Vertex> stateList, List<string> finalStateList, string startingState, List<Edge> edgeList)
        {
            textWritter = new XmlTextWriter(path, Encoding.UTF8);
            textWritter.WriteStartDocument();
            textWritter.WriteStartElement("graph");
            textWritter.WriteEndElement();
            textWritter.Close();
            doc.Load(path);
            int count = 1;
            foreach (Vertex state in stateList)
                if (!finalStateList.Contains(state.Name) && state.Name != startingState)
                {
                    AppendState(state.Name, count, false, false);
                    stateID.Add(state.Name, "state" + count);
                    count++;
                }
            foreach (string state in finalStateList)
                if (state != startingState)
                {
                    AppendState(state, count, true, false);
                    stateID.Add(state, "state" + count);
                    count++;
                }
            AppendState(startingState, count, false, true);
            stateID.Add(startingState, "state" + count);
            count = 1;
            foreach (Edge edge in edgeList)
            {
                AppendEdge(edge.Transition, count, stateID[edge.Source.Name], stateID[edge.Target.Name]);
                count++;
            }
            doc.Save(path);
        }
        /// <summary>
        /// Метод, получающий список состояний автомата из корневого элемента tsml файла.
        /// </summary>
        /// <param name="root">Корневой элемент.</param>
        /// <returns>Список состяний.</returns>
        public List<TSMlVertex> GetNodeList(XmlElement root)
        {
            finalStates = new List<string>();
            List<TSMlVertex> nodes = new List<TSMlVertex>();
            foreach (XmlNode xnode in root)
            {
                if (xnode.Name=="state")
                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                    if (childnode.Name == "name")
                        {
                            XmlNode attr1 = xnode.Attributes.GetNamedItem("start");
                            if (attr1 != null)
                            {
                                startingState = childnode.InnerText;
                                graph.UserAddVertix("");
                                graph.UserAddEdge("", startingState, "");
                            }
                            XmlNode attr2 = xnode.Attributes.GetNamedItem("accept");
                            if (attr2 != null)
                                finalStates.Add(childnode.InnerText);
                            XmlNode attr = xnode.Attributes.GetNamedItem("id");
                            //MessageBox.Show("id=" + attr.Value, childnode.InnerText);
                            nodes.Add(new TSMlVertex { id = attr.Value, name = childnode.InnerText });
                        graph.UserAddVertix(childnode.InnerText);
                    }
                }
            }
            return nodes;
        }
        /// <summary>
        /// Метод, получающий список переходов автомата из корневого элемента tsml файла.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="nodes"></param>
        public void GetEdgeList(XmlElement root, List<TSMlVertex> nodes)
        {
            string source;
            string target;
            string text;
            XmlNode attr;
            foreach (XmlNode xnode in root)
            {
                if (xnode.Name == "transition")
                    foreach (XmlNode childnode in xnode.ChildNodes)
                    {
                        if (childnode.Name == "name")
                        {
                            attr = xnode.Attributes.GetNamedItem("source");
                            source=nodes.Find(item => item.id == attr.Value).name;
                            attr = xnode.Attributes.GetNamedItem("target");
                            target = nodes.Find(item => item.id == attr.Value).name;
                            text = childnode.InnerText;
                            graph.UserAddEdge(source,target,text.Replace("+complete"," ").Replace("+"," "));
                        }
                    }
            }
        }
        /// <summary>
        /// Метод, записывающий в файл формата .tsml информацию о состоянии автомата.
        /// </summary>
        /// <param name="name">Имя состояния.</param>
        /// <param name="num">Номер состяония.</param>
        /// <param name="isFinal">Конечное ли состояние.</param>
        /// <param name="isStarting">Начальное ли состояние.</param>
        public void AppendState(string name, int num, bool isFinal, bool isStarting)
        {
            XmlAttribute attribute;
            XmlNode element;
            element = doc.CreateElement("state");
            doc.DocumentElement.AppendChild(element);
                attribute = doc.CreateAttribute("id"); // создаём атрибут
                attribute.Value = "state"+num.ToString(); // устанавливаем значение атрибута
                element.Attributes.Append(attribute); // добавляем атрибут
            XmlNode childNode = doc.CreateElement("name");
            childNode.InnerText = name;
            element.AppendChild(childNode);
            if (isFinal)
            {
                attribute = doc.CreateAttribute("accept"); // создаём атрибут
                attribute.Value = "true"; // устанавливаем значение атрибута
                element.Attributes.Append(attribute); // добавляем атрибут
            }
            if (isStarting)
            {
                attribute = doc.CreateAttribute("start"); // создаём атрибут
                attribute.Value = "true"; // устанавливаем значение атрибута
                element.Attributes.Append(attribute); // добавляем атрибут
            }
            //element.InnerText = name;
        }
        /// <summary>
        /// Метод, записывающий в файл формата .tsml информацию о переходе автомата.
        /// </summary>
        /// <param name="name">Символ перехода.</param>
        /// <param name="num">Номер перехода.</param>
        /// <param name="source">Состояние исхода.</param>
        /// <param name="target">Состояние входа.</param>
        public void AppendEdge(string name, int num, string source, string target)
        {
            XmlAttribute attribute;
            XmlNode element;
            element = doc.CreateElement("transition");
            doc.DocumentElement.AppendChild(element);
            attribute = doc.CreateAttribute("id"); // создаём атрибут
            attribute.Value = "transition" + num.ToString(); // устанавливаем значение атрибута
            element.Attributes.Append(attribute); // добавляем атрибут
            attribute = doc.CreateAttribute("source");
            attribute.Value = source;
            element.Attributes.Append(attribute);
            attribute = doc.CreateAttribute("target");
            attribute.Value = target;
            element.Attributes.Append(attribute);
            XmlNode childNode = doc.CreateElement("name");
            childNode.InnerText = name;
            element.AppendChild(childNode);
            //element.InnerText = name;
        }
    }
   
}
