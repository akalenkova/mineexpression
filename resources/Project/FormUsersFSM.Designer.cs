﻿namespace Project
{
    partial class FormUsersFSM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUsersFSM));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonConstructExpression = new System.Windows.Forms.Button();
            this.buttonConstructWithAnimation = new System.Windows.Forms.Button();
            this.pictureBoxUsersFSM = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUsersFSM)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1554, 40);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.newProjectToolStripMenuItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(155, 36);
            this.newProjectToolStripMenuItem.Text = "New Project";
            this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.newProjectToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(92, 36);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // buttonConstructExpression
            // 
            this.buttonConstructExpression.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonConstructExpression.Font = new System.Drawing.Font("Perpetua Titling MT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConstructExpression.ForeColor = System.Drawing.Color.DodgerBlue;
            this.buttonConstructExpression.Location = new System.Drawing.Point(0, 80);
            this.buttonConstructExpression.Name = "buttonConstructExpression";
            this.buttonConstructExpression.Size = new System.Drawing.Size(210, 137);
            this.buttonConstructExpression.TabIndex = 2;
            this.buttonConstructExpression.Text = "Construct a Regular Expression";
            this.buttonConstructExpression.UseVisualStyleBackColor = false;
            this.buttonConstructExpression.Click += new System.EventHandler(this.buttonConstructExpression_Click);
            // 
            // buttonConstructWithAnimation
            // 
            this.buttonConstructWithAnimation.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonConstructWithAnimation.Font = new System.Drawing.Font("Perpetua Titling MT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConstructWithAnimation.ForeColor = System.Drawing.Color.DodgerBlue;
            this.buttonConstructWithAnimation.Location = new System.Drawing.Point(0, 223);
            this.buttonConstructWithAnimation.Name = "buttonConstructWithAnimation";
            this.buttonConstructWithAnimation.Size = new System.Drawing.Size(210, 163);
            this.buttonConstructWithAnimation.TabIndex = 3;
            this.buttonConstructWithAnimation.Text = "Construct a Regular Expression With Animation";
            this.buttonConstructWithAnimation.UseVisualStyleBackColor = false;
            this.buttonConstructWithAnimation.Click += new System.EventHandler(this.buttonConstructWithAnimation_Click);
            // 
            // pictureBoxUsersFSM
            // 
            this.pictureBoxUsersFSM.Location = new System.Drawing.Point(216, 80);
            this.pictureBoxUsersFSM.Name = "pictureBoxUsersFSM";
            this.pictureBoxUsersFSM.Size = new System.Drawing.Size(1277, 973);
            this.pictureBoxUsersFSM.TabIndex = 4;
            this.pictureBoxUsersFSM.TabStop = false;
            // 
            // FormUsersFSM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1554, 1103);
            this.Controls.Add(this.pictureBoxUsersFSM);
            this.Controls.Add(this.buttonConstructWithAnimation);
            this.Controls.Add(this.buttonConstructExpression);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormUsersFSM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expression Miner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormUsersFSM_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUsersFSM)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.Button buttonConstructExpression;
        private System.Windows.Forms.Button buttonConstructWithAnimation;
        private System.Windows.Forms.PictureBox pictureBoxUsersFSM;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}