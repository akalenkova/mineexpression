﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Project
{
    /// <summary>
    /// Класс, хранящий определения процессов и подпроцессов.
    /// </summary>
    public partial class FormProcessDefinitions : Form
    {
        /// <summary>
        /// Средняя длина подпроцессов.
        /// </summary>
        public string MidLength { get { return labelAverageLength.Text; } }
        /// <summary>
        /// Содержимое таблицы - словаря определений.
        /// </summary>
        public List<string> tableContents
        {
            get
            {
                List<string> contents = new List<string>();
                for (int count = 0; count < dataGridViewDefinitions.RowCount; count++)
                {
                    string str = $"{count + 1}. " + dataGridViewDefinitions.Rows[count].Cells[0].Value 
                        + " = " + Change(dataGridViewDefinitions.Rows[count].Cells[1].Value.ToString());
                    if (count != dataGridViewDefinitions.RowCount - 1)
                        str += ";";
                    else
                        str += ".";
                    contents.Add(str);
                }
                return contents;
            }
        }
        /// <summary>
        /// Заполнение таблицы словаря определениями процессов.
        /// </summary>
        /// <param name="basics">Словарь процессов.</param>
        public FormProcessDefinitions(Dictionary<string, string> basics)
        {
            InitializeComponent();
            panel1.Width = Width;
            dataGridViewDefinitions.Width = Width - 35;
            dataGridViewDefinitions.Height = Height - panel1.Height - 65;
            label2.Text = "";
            labelAverageLength.Text = "";
            dataGridViewDefinitions.RowCount = basics.Count;
            dataGridViewDefinitions.ColumnCount = 2;
            dataGridViewDefinitions.Columns[0].Name = "Process";
            dataGridViewDefinitions.Columns[1].Name = "Definition";
            int i = 0;
            foreach (string key in basics.Keys)
            {
                dataGridViewDefinitions.Rows[i].Cells[0].Value = key;
                dataGridViewDefinitions.Rows[i].Cells[1].Value = Change(basics[key]);
                i++;
            }

        }
        /// <summary>
        /// Заполнение таблицы словаря определениями подпроцессов.
        /// </summary>
        /// <param name="basics">Словарь процессов</param>
        /// <param name="finals">Словарь подпроцессов.</param>
        public FormProcessDefinitions(Dictionary<string,string> basics, Dictionary<string,string> finals)
        {
            InitializeComponent();
            panel1.Width = Width;
            dataGridViewDefinitions.Width = Width - 35;
            dataGridViewDefinitions.Height = Height - panel1.Height - 65;
            dataGridViewDefinitions.RowCount = finals.Count;
            dataGridViewDefinitions.ColumnCount = 2;
            dataGridViewDefinitions.Columns[0].Name = "Process";
            dataGridViewDefinitions.Columns[1].Name = "Definition";
            Dictionary<string, string> contents = new Dictionary<string, string>();
            foreach (string key in finals.Keys)
                contents.Add(key, Change(finals[key]));
            float count = 0;
            foreach (string key in contents.Keys)
         {
             string str = contents[key];
             char[] array = str.ToCharArray();
                    foreach (char c in array)
                    {
                        if (c != '+' && c != '.' && c != ')' && c != '(' && c != '*')
                        count++;
                    }
            }
            try
            {
                count /= contents.Count;
            }
            catch(DivideByZeroException)
            {
                count = 0;
            }
            if (dataGridViewDefinitions.RowCount == 0)
                count = 0;
            labelAverageLength.Text = count.ToString();
            int i = 0;
            foreach (string key in contents.Keys)
            {
                dataGridViewDefinitions.Rows[i].Cells[0].Value = key;
                dataGridViewDefinitions.Rows[i].Cells[1].Value = Change(finals[key]);
                i++;
            }
        }
        /// <summary>
        /// Изменение регулярного выражения. (Удаление символов, добавленных вместе с единым конечным состоянием).
        /// </summary>
        /// <param name="expression">Регулярное выражение.</param>
        /// <returns></returns>
        string Change(string expression)
        {
            string result = "";
            char[] expArr = expression.ToCharArray();
            for (int i=0;i<expArr.Length;i++)
            {
                if (expArr[i] == '&' &&i!=0&&result[result.Length - 1] == '.')
                    result = result.Remove(result.Length - 1);
                else if (expArr[i] == '+' && i != 0 && result[result.Length - 1] == '&')
                {
                    result = result.Remove(result.Length - 1);
                    result += (char)400;
                    result += expArr[i];
                }
                else
                    result += expArr[i];
            }
            return result;
        }
        /// <summary>
        /// Обработка закрытие формы. (Объект формы не должен быть удален).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormProcessDefinitions_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.ApplicationExitCall)
            {
                e.Cancel = true;
                Visible = false;
            }
        }
        /// <summary>
        /// Обработка изменения размеров формы.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormProcessDefinitions_SizeChanged(object sender, EventArgs e)
        {
            panel1.Width = Width;
            dataGridViewDefinitions.Width = Width-35;
            dataGridViewDefinitions.Height = Height-panel1.Height-65;
        }
    }
}
