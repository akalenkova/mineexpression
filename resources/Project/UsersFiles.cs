﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
namespace Project
{
    /// <summary>
    /// Класс, оперирующий работой с файлами.
    /// </summary>
    class UsersFiles
    {
        /// <summary>
        /// Файловый поток.
        /// </summary>
        Stream myStream = null;
        /// <summary>
        /// Поле, хранящее путь к открытому пользоватем файлу.
        /// </summary>
        string filePath;
        /// <summary>
        /// Диалоговое окно открытия файла.
        /// </summary>
        OpenFileDialog myOpenFileDialog;
        /// <summary>
        /// Свойство доступа к полю filePath.
        /// </summary>
        public string FilePath { get { return filePath; } }
        /// <summary>
        /// Метод, удаляющий папку, созданную во время работы программы.
        /// </summary>
        public void DeleteFiles()
        {
            if(Directory.Exists("Graphs"))
            Directory.Delete("Graphs",true);
        }
        /// <summary>
        /// Метод, получающий путь к открытому пользователем файлу.
        /// </summary>
        /// <returns></returns>
        public bool OpenFile()
        {
            myOpenFileDialog = new OpenFileDialog();
            //myOpenFileDialog.InitialDirectory = "c:\\";
            myOpenFileDialog.RestoreDirectory = true;
            myOpenFileDialog.Filter = "tsml-файл (*.tsml)|*.tsml";
            if (myOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                filePath = Path.GetFullPath(myOpenFileDialog.FileName);
                try
                {
                    myStream = myOpenFileDialog.OpenFile();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error! File is not found! " + ex.Message);
                }
                return true;
            }
            else
            {
                MessageBox.Show("Error! File is not opened! ");
                return false;
            }
        }
        /// <summary>
        /// Метод, получающий путь к файлу, в который нужно сохранить информацию. 
        /// </summary>
        /// <param name="format">Формат сохраняемого файла.</param>
        /// <returns></returns>
        public string ChooseSavingPath(string format)
        {
            string path = "";
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            switch (format)
            {
                case "tsml":
                    saveFileDialog.Filter = "tsml-file (*.tsml)|*.tsml";
                    break;
                case "png":
                    saveFileDialog.Filter = "png-file (*.png)|*.png";
                    break;
                case "txt":
                    saveFileDialog.Filter = "txt-file (*.txt)|*.txt";
                    break;
            }
            saveFileDialog.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)//создаем новый файл
            {
                try
                {
                    if ((myStream = saveFileDialog.OpenFile()) != null)
                    {
                        myStream.Close();
                        path = Path.GetFullPath(saveFileDialog.FileName);
                    }
                }
                catch (IOException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return path;
        }
        /// <summary>
        /// Метод, осуществляющий сохранение изображения.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="bmp"></param>
        public void SaveImage(string path, Bitmap bmp)
        {
                bmp.Save(path, System.Drawing.Imaging.ImageFormat.Png);
        }
        /// <summary>
        /// Метод, осуществляющий сохраниние аналиических данных, полученных в результате работы алгоритма.
        /// </summary>
        /// <param name="afterAlgo"></param>
        /// <param name="beforeAlgo"></param>
        /// <param name="basicDefinitions"></param>
        /// <param name="processDefinitions"></param>
        /// <param name="analytics"></param>
        public void SaveMinedExpression(List<string> afterAlgo, List<string> beforeAlgo, List<string> basicDefinitions, List<string> processDefinitions, List<string> analytics)
        {
            string path = ChooseSavingPath("txt");
            if (path == "")
                MessageBox.Show("Error! File was not saved...");
            else
            {
                using (StreamWriter stream = new StreamWriter(path))
                {
                    foreach (string str in afterAlgo)
                        stream.WriteLine(str);
                    foreach (string str in beforeAlgo)
                        stream.WriteLine(str);
                    foreach(string str in basicDefinitions)
                        stream.WriteLine(str);
                    foreach(string str in processDefinitions)
                        stream.WriteLine(str);
                    foreach(string str in analytics)
                        stream.WriteLine(str);
                }
            }
        }
    }
}
