﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuickGraph;
using QuickGraph.Graphviz;
using QuickGraph.Graphviz.Dot;
using System.IO;
using System.Diagnostics;

namespace LibGraph

{
    /// <summary>
    /// Класс, участвующий в получении изображения из конечного автомата.
    /// </summary>
    public class FileDotEngine : IDotEngine
    {
        /// <summary>
        /// Получение изображения автомата.
        /// </summary>
        /// <param name="imageType"></param>
        /// <param name="dot"></param>
        /// <param name="outputFileName"></param>
        /// <returns></returns>
        public string Run(GraphvizImageType imageType, string dot, string outputFileName)
        {
            using (StreamWriter writer = new StreamWriter(outputFileName))
            {
                writer.Write(dot);
            }
            return Path.GetFileName(outputFileName);
        }
    }
    /// <summary>
    /// Класс, задающий конечный автомат.
    /// </summary>
    public class GraphBuilder

    {
        /// <summary>
        /// Количество совершенных перобразований автомата.
        /// </summary>
        public int NumberOfGraphs { get; private set; }
        /// <summary>
        /// Поле, список конечных состояний.
        /// </summary>
        List<string> finalStates=new List<string>();
        /// <summary>
        /// Свойство, список конечных состояний.
        /// </summary>
        public List<string> FinalStates { get { return finalStates; } }
        /// <summary>
        /// Словарь процессов.
        /// </summary>
        public Dictionary<string, string> basicDefinitions = new Dictionary<string, string>();
        /// <summary>
        /// Словарь подпроцессов.
        /// </summary>
        public Dictionary<string, string> finalDefinitions = new Dictionary<string, string>();
        /// <summary>
        /// Пользовательский конечный автомат.
        /// </summary>
        public AdjacencyGraph<string, TaggedEdge<string, string>> myGraph = new AdjacencyGraph<string, TaggedEdge<string, string>>();
        /// <summary>
        /// Метод, инициализирующий список конечных состояний.
        /// </summary>
        /// <param name="finStates">Список конечных состояний.</param>
        public void InitializeFinalStates(List<string> finStates)
        {
            finalStates = finStates;
        }
        /// <summary>
        /// Метод, объединяющий все петли состояния.
        /// </summary>
        public void ConcatSelfLoops()
        {
            foreach (string vertex in myGraph.Vertices)
            {
                List<string> loopsText = new List<string>();
                int outDegree = myGraph.OutDegree(vertex);
                for (int i = 0; i < outDegree; i++)
                {
                    foreach (var edge in myGraph.OutEdges(vertex))
                    {
                        if (edge.Target == vertex)
                        {
                            loopsText.Add(edge.Tag);
                            myGraph.RemoveEdge(edge);
                            break;
                        }
                    }
                }
                string edgeText = "";
                if (loopsText.Count != 0)
                {
                    if (loopsText.Count == 1)
                        edgeText = loopsText.First();
                    else
                    {
                        int num = 0;
                        foreach (string text in loopsText)
                        {
                            num++;
                            edgeText += text;
                            if (num != loopsText.Count)
                                edgeText += "+";
                        }
                    }
                    UserAddEdge(vertex, vertex, edgeText);
                }
            }
        }
        /// <summary>
        /// Метод, добавляющий в автомат состояние.
        /// </summary>
        /// <param name="nameVert">Имя состояния.</param>
        public void UserAddVertix(string nameVert)
        {
            myGraph.AddVertex(nameVert);
        }
        /// <summary>
        /// Метод, удаляющий конечное состояние автомата из списка.
        /// </summary>
        /// <param name="nameVert">Имя состояния.</param>
        public void UserRemoveFinalVertix(string nameVert)
        {
            finalStates.Remove(nameVert);
        }
        /// <summary>
        /// Метод, добавляющий в автомат переход.
        /// </summary>
        /// <param name="startVert">Вершина исхода.</param>
        /// <param name="endVert">Вершина входа.</param>
        /// <param name="edgeText">Символ перехода.</param>
        public void UserAddEdge(string startVert, string endVert, string edgeText)
        {
            myGraph.AddEdge(new TaggedEdge<string, string>(startVert, endVert, edgeText));
        }
        /// <summary>
        /// Метод, добавляющий в автомат конечное состояние.
        /// </summary>
        /// <param name="nameVert">Имя состояния.</param>
        public void UserAddFinalVertix(string nameVert)
        {
            myGraph.AddVertex(nameVert);
            finalStates.Add(nameVert);
        }
        /// <summary>
        /// Метод, преобразующий описание графа в картинку.
        /// </summary>
        /// <param name="graphNum"></param>
        /// <returns></returns>
        public string MakeDot(int graphNum)
        {
            if (graphNum != -1)
                NumberOfGraphs = graphNum;
            var graphViz = new GraphvizAlgorithm<string, TaggedEdge<string, string>>(myGraph);
            graphViz.FormatVertex += FormVertex;
            graphViz.FormatEdge += FormEdge;
            string name = String.Format("graphviz{0}",graphNum);
            var dotPath = Directory.GetCurrentDirectory()+Path.DirectorySeparatorChar+"Graphs"+Path.DirectorySeparatorChar+ name + ".dot";
            graphViz.Generate(new FileDotEngine(), dotPath);
            var dotExePath = @"Graphviz.2.38.0.2\dot.exe";
             if (File.Exists(dotExePath))
             {
                var psi = new ProcessStartInfo(dotExePath);
                psi.Arguments = String.Format( "-Tpng -o \"{0}\" \"{1}\"",Path.ChangeExtension(dotPath, "png"),dotPath);
                psi.UseShellExecute = false;
                psi.RedirectStandardOutput = true;
                psi.CreateNoWindow = true;
                var p = Process.Start(psi);
                var stdout = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
            }
            return Path.ChangeExtension(dotPath, "png");
        }
        /// <summary>
        /// Метод, задающий формат вершины графа.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="vert"></param>
        private void FormVertex(object sender, FormatVertexEventArgs<string> vert)
        {
            bool isFinal = false;
            foreach (string fin in finalStates)
                if (vert.Vertex == fin)
                    isFinal = true;
            vert.VertexFormatter.Label = vert.Vertex;
            if(isFinal)
                vert.VertexFormatter.Shape = GraphvizVertexShape.DoubleCircle;
            else
                vert.VertexFormatter.Shape = GraphvizVertexShape.Circle;
                vert.VertexFormatter.StrokeColor = GraphvizColor.Black;
                vert.VertexFormatter.FillColor = GraphvizColor.LightYellow;
                vert.VertexFormatter.Style = GraphvizVertexStyle.Bold;
                vert.VertexFormatter.Font =new GraphvizFont("Calibri", 12);

        }
        /// <summary>
        /// Метод, задающий формат ребра графа.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="edge"></param>
        private void FormEdge(object sender, FormatEdgeEventArgs<string, TaggedEdge<string, string>> edge)
        {
                edge.EdgeFormatter.Label = new GraphvizEdgeLabel { Value = edge.Edge.Tag };
                edge.EdgeFormatter.Font = new GraphvizFont("TimesNewRoman", 12-edge.Edge.Tag.Length/60);
                edge.EdgeFormatter.FontGraphvizColor = GraphvizColor.Black;
                edge.EdgeFormatter.StrokeGraphvizColor = GraphvizColor.Black;
        }
    }
}