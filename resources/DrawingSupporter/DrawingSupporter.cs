﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace DrawingSupporter
{
    /// <summary>
    /// Класс, оперирующий отрисовкой элементов создаваемого пользователем конечного автомата.
    /// </summary>
    public class DrawAssistant
    {
        Bitmap bitmap;
        Pen blackPen = new Pen(Color.Black);
        Pen bluePen = new Pen(Color.Blue);
        Pen redPen = new Pen(Color.Red);
        Pen greenPen = new Pen(Color.Green);
        Pen brownPen = new Pen(Color.Brown);
        Graphics graphics;
        Point point;
        Font vertexFont;
        Font edgeFont;
        /// <summary>
        /// Список ребер графа.
        /// </summary>
        List<Edge> edgeList;
        /// <summary>
        /// Список вершин графа.
        /// </summary>
        List<Vertex> vertexList;
        /// <summary>
        /// Список конечных состояний графа.
        /// </summary>
        List<string> finalsList;
        /// <summary>
        /// Начальное состояние графа.
        /// </summary>
        string startingState="";
        /// <summary>
        /// Радиус вершины графа.
        /// </summary>
        int Rad = 20;
        /// <summary>
        /// Свойство, возвращающее начальное состояние автомата.
        /// </summary>
        public string StartingState { set { startingState = value; } }
        /// <summary>
        /// Свойство, возвращающее радиус состояния автомата.
        /// </summary>
        public int Radius { get { return Rad; } }
        /// <summary>
        /// Свойство, хранящее изображение построенного автомата.
        /// </summary>
        public Bitmap Image { get { return bitmap; } }
        /// <summary>
        /// Свойство, хранящее список конечных состояний автомата.
        /// </summary>
        public List<string> Finals { get { return finalsList; } }
        public DrawAssistant(int width, int height, List<Edge> edgeList, List<Vertex> vertexList, List<string> finals)
        {
            bitmap = new Bitmap(width, height);
            graphics = Graphics.FromImage(bitmap);
            vertexFont = new Font("Times New Roman", 16, FontStyle.Bold);
            edgeFont = new Font("Times New Roman", 16);
            this.edgeList = edgeList;
            this.vertexList = vertexList;
            finalsList = finals;
        }
        /// <summary>
        /// Метод, изменяющий список конечных состояний автомата.
        /// </summary>
        /// <param name="name">Имя состояния</param>
        public void ChangeFinalsList(string name)
        {
            if (name != startingState)
            {
                if (finalsList.Contains(name))
                    finalsList.Remove(name);
                else
                    finalsList.Add(name);
            }
        }
        /// <summary>
        /// Метод, выделяющий вершину, выбранную как исходную для ребра.
        /// </summary>
        /// <param name="x">Координата вершины по оси Х.</param>
        /// <param name="y">Координата вершины по оси Y.</param>
        /// <param name="name">Название вершины.</param>
        public void DrawSelectedVertex(int x, int y,string name)
        {
            graphics.FillEllipse(Brushes.Aqua, x - Rad, y - Rad, 2 * Rad, 2 * Rad);
            graphics.DrawEllipse(bluePen, x - Rad, y - Rad, 2 * Rad, 2 * Rad);
            point = new Point(x - 5 * Rad / 12, y - 6 * Rad / 12);
            graphics.DrawString(name, vertexFont, Brushes.Blue, point);
        }
        /// <summary>
        /// Метод, выделяющий вершину, выбранную для удаления.
        /// </summary>
        /// <param name="x">Координата вершины по оси Х.</param>
        /// <param name="y">Координата вершины по оси Y.</param>
        /// <param name="name">Название вершины.</param>
        public void DrawVertexToDelete(int x, int y, string name)
        {
            graphics.FillEllipse(Brushes.Pink, x - Rad, y - Rad, 2 * Rad, 2 * Rad);
            graphics.DrawEllipse(redPen, x - Rad, y - Rad, 2 * Rad, 2 * Rad);
            point = new Point(x - 5 * Rad / 12, y - 6 * Rad / 12);
            graphics.DrawString(name, vertexFont, Brushes.Red, point);
        }
        /// <summary>
        /// Метод, выполняющий отрисовку вершины.
        /// </summary>
        /// <param name="x">Координата вершины по оси Х.</param>
        /// <param name="y">Координата вершины по оси Y.</param>
        /// <param name="name">Название вершины.</param>
        public void DrawVertex(int x, int y, string name)
        {
            if (name == startingState)//если начальная вершина - рисуем зеленым
            {
                graphics.FillEllipse(Brushes.LightGreen, x - Rad, y - Rad, 2 * Rad, 2 * Rad);
                graphics.DrawEllipse(greenPen, x - Rad, y - Rad, 2 * Rad, 2 * Rad);
            }
            else if (finalsList.Contains(name))//если вершина из списка конечных состояний - рисуем желтым
            {
                graphics.FillEllipse(Brushes.LightYellow, x - Rad, y - Rad, 2 * Rad, 2 * Rad);
                graphics.DrawEllipse(brownPen, x - Rad, y - Rad, 2 * Rad, 2 * Rad);
            }
            else//если обычная вершина - рисуем в черно-белом стиле
            {
                graphics.FillEllipse(Brushes.White, x - Rad, y - Rad, 2 * Rad, 2 * Rad);
                graphics.DrawEllipse(blackPen, x - Rad, y - Rad, 2 * Rad, 2 * Rad);
            }
            point = new Point(x - 5 * Rad / 12, y - 6 * Rad / 12);
            graphics.DrawString(name, vertexFont, Brushes.Black, point);
        }
        /// <summary>
        /// Метод, выполняющий отрисовку ребра.
        /// </summary>
        /// <param name="x1">Координата вершины исхода по оси Х.</param>
        /// <param name="y1">Координата вершины исхода по оси Y.</param>
        /// <param name="x2">Координата вершины входа по оси Х.</param>
        /// <param name="y2">Координата вершины входа по оси Y.</param>
        /// <param name="name">Символ перехода.</param>
        /// <param name="edge">Само ребро, как линия, соединяющая две вершины.</param>
        public void DrawEdge(int x1, int y1, int x2, int y2, string name, Edge edge)
        {
            Pen pen = new Pen(Color.Black, 1);
            pen.StartCap = LineCap.RoundAnchor;
            pen.EndCap = LineCap.ArrowAnchor;
            if (x1 != x2 && y1 != y2)
            {
                int startX = 0, startY = 0, finX = 0, finY = 0;
                Point p1 = new Point(x2 - x1, y2 - y1);
                int k1 = (int)(Rad * (x2 - x1) / Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2)));
                int k2 = (int)(Rad * (y2 - y1) / Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2)));
                if (!ReverseEdge(edge))
                {
                    startX = x1 + k1;
                    startY = y1 + k2;
                    finX = x2 - k1;
                    finY = y2 - k2;
                    graphics.DrawLine(pen, startX, startY, finX, finY);
                    point = new Point((x1 + x2 - k1) / 2, (y1 + y2 - k1) / 2);
                }
                else
                {
                    if (y1 < y2)
                    {
                        startY = y1;
                        finY = y2;
                        if (x1 > x2)
                        {
                            startX = x1 - Rad;
                            finX = x2 + Rad;
                        }
                        if (x2 >= x1)
                        {
                            startX = x1 + Rad;
                            finX = x2 - Rad;
                        }
                    }
                    if (y1 > y2)
                    {
                        if (x1 == x2)
                        {
                            startY = y1;
                            finY = y2;
                            startX = x1 + Rad;
                            finX = x2 - Rad;
                        }
                        else
                        {
                            startX = x1;
                            finX = x2;
                            startY = y1 - Rad;
                            finY = y2 + Rad;
                        }
                    }
                    if (y1 == y2)
                    {
                        startX = x1;
                        finX = x2;
                        startY = y1 - Rad;
                        finY = y2 + Rad;
                    }
                    graphics.DrawLine(pen, startX, startY, finX, finY);
                    point = new Point(startX + (finX - startX) / 3, startY + (finY - startY) / 3);
                }
            }
            else
            {
                int x = x1 - 20 * Rad / 12;
                int y = y1 - Rad;
                float width = 2 * Rad;
                float height = 22 * Rad / 12;
                graphics.DrawArc(pen, x,y, width, height, 70, 360);
                point = new Point(x1 - 5 * Rad / 2, y1 - Rad);
            }
            graphics.DrawString(name, edgeFont, Brushes.Black, point);
        }
        /// <summary>
        /// Метод, выполняющий отрисовку графа по списку ребер и вершин.
        /// </summary>
        public void Draw()
        {
            ClearSheet();
            foreach (Edge edge in edgeList)
                DrawEdge(edge.Source.X, edge.Source.Y, edge.Target.X, edge.Target.Y, edge.Transition, edge);
            foreach (Vertex vert in vertexList)
                DrawVertex(vert.X, vert.Y, vert.Name);
        }
        /// <summary>
        /// Метод, определяющий, есть ли обратное к данному ребро.
        /// </summary>
        /// <param name="edge">Само ребро, как линия, соединяющая две вершины.</param>
        /// <returns></returns>
        bool ReverseEdge(Edge edge)
        {
            foreach (Edge e in edgeList)
                if (edge.Source == e.Target && edge.Target == e.Source)
                    return true;
            return false;
        }
        /// <summary>
        /// Метод, очищающий лист для рисования.
        /// </summary>
        public void ClearSheet()
        {
            graphics.Clear(Color.White);
        }
        /// <summary>
        /// Метод, удаляющий вершину из списка вершин.
        /// </summary>
        /// <param name="vertex">Вершина графа, как имя и пара координат.</param>
        public void DeleteVertex(Vertex vertex)
        {
            vertexList.Remove(vertex);
            bool found = false;
            do
            {
                found = false;
                foreach (var edge in edgeList)
                    if (edge.Source.Name == vertex.Name || edge.Target.Name == vertex.Name)
                    {
                        found = true;
                        edgeList.Remove(edge);
                        break;
                    }
            } while (found);
            Draw();
        }
        /// <summary>
        /// Метод, удаляющий ребро из списка ребер.
        /// </summary>
        /// <param name="edge">Само ребро, как линия, соединяющая две вершины.</param>
        public void DeleteEdge(Edge edge)
        {
            edgeList.Remove(edge);
            Draw();
        }
        /// <summary>
        /// Метод, определяющий, нажал ли пользователь на ребро.
        /// </summary>
        /// <param name="edge">Ребро, на которое нажал пользователь. (Имеет значение null, если не было нажато ни одно ребро).</param>
        /// <param name="x">Координата мыши по оси Х.</param>
        /// <param name="y">Координата мыши по оси Y.</param>
        /// <returns></returns>
        public bool EdgeHitten(ref Edge edge, int x, int y)
        {
            foreach (Edge e in edgeList)
            {
                if (CheckEdgeHitten(e,x,y))
                {
                    edge = e;
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Метод, определяющий, принадлежит ли данная точка ребру.
        /// </summary>
        /// <param name="edge">Проверяемое ребро.</param>
        /// <param name="x">Координата мыши по оси Х.</param>
        /// <param name="y">Координата мыши по оси Y.</param>
        /// <returns></returns>
        public bool CheckEdgeHitten(Edge edge, int x, int y)
        {
            int x1 = edge.Source.X;
            int y1 = edge.Source.Y;
            if (edge.Source==edge.Target)
            {
                if (Math.Pow(x - x1, 2) + Math.Pow(y - y1, 2) <= 4 * Rad*Rad)
                    return true;
            }
            else
            {
                int x2 = edge.Target.X;
                int y2 = edge.Target.Y;
                float catheter = (y2 - y1) * (x - x1) /(x2 - x1) + y1;
                if (catheter>=y-4&&catheter<=y+4)
                {
                    if (x >= x2 && x <= x1 || x >= x1 && x <= x2)
                        return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Метод, определяющий, нажал ли пользователь на вершину.
        /// </summary>
        /// <param name="vertex">Вершина, на которую нажал пользователь. (Имеет значение null, если не была нажата ни одна вершина).</param>
        /// <param name="x">Координата мыши по оси Х.</param>
        /// <param name="y">Координата мыши по оси Y.</param>
        /// <param name="vertices">Список вершин.</param>
        /// <returns></returns>
        public bool VertexHitten(ref Vertex vertex, int x, int y, List<Vertex> vertices)
        {
            foreach (var vert in vertices)
                if (Math.Pow(vert.X - x, 2) + Math.Pow(vert.Y - y, 2) <= Rad * Rad)
                {
                    vertex = vert;
                    return true;
                }
            return false;
        }
    }
        /// <summary>
        /// Класс, задающий вершину графа, как имя и пару координат.
        /// </summary>
        public class Vertex
        {
        /// <summary>
        /// Имя состояния.
        /// </summary>
            public string Name { get; set; }
        /// <summary>
        /// Координата вершины графа по оси Х.
        /// </summary>
            public int X { get; set; }
        /// <summary>
        /// Координата вершины графа по оси Y.
        /// </summary>
        public int Y { get; set; }

            public Vertex(string name, int x, int y)
            {
                Name = name;
                X = x;
                Y = y;
            }
        }
    /// <summary>
    /// Класс, задающий переход, как символ перехода и два состояния, как вершины графа.
    /// </summary>
        public class Edge
        {
        /// <summary>
        /// Символ перехода.
        /// </summary>
            public string Transition { get; set; }
        /// <summary>
        /// Вершина исхода.
        /// </summary>
            public Vertex Source { get; set; }
        /// <summary>
        /// Вершина входа.
        /// </summary>
            public Vertex Target { get; set; }
            public Edge(Vertex source, Vertex target, string tag)
            {
                Transition = tag;
                Source = source;
                Target = target;
            }
        }
}
