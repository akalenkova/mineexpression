﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuickGraph;
using LibGraph;

namespace DeletingAlgo
{
    /// <summary>
    /// Класс, представляющий пару состояние - переход, используется в списке исходящих дуг.
    /// </summary>
    public class OutEdgeList
    {
        /// <summary>
        /// Регулярное выражение, задающее переход.
        /// </summary>
        public string EdgeLabel { get; set; }
        /// <summary>
        /// Состояние входа.
        /// </summary>
        public string TargetVertex { get; set; }
    }
    /// <summary>
    /// Класс, представляющий пару состояние - переход, используется в списке входящих дуг.
    /// </summary>
    public class InEdgeList
    {
        /// <summary>
        /// Регулярное выражение, задающее переход.
        /// </summary>
        public string EdgeLabel { get; set; }
        /// <summary>
        /// Состояние исхода.
        /// </summary>
        public string SourceVertex { get; set; }
    }
    /// <summary>
    /// Класс, который оперирует удалением состояний.
    /// </summary>
    public class StateRemoval
    {
        Random rand = new Random();
        /// <summary>
        /// Поле, задающее список исходящих из состояния дуг
        /// </summary>
        List<OutEdgeList> exitingEdges = new List<OutEdgeList>();
        /// <summary>
        /// Поле, задающее список входящих в состояние дуг
        /// </summary>
        List<InEdgeList> enteringEdges = new List<InEdgeList>();
        /// <summary>
        /// Поле, задающее список неудаляемых состояний.
        /// </summary>
        List<string> untouchedStatesList;
        /// <summary>
        /// Поле, задающее начальное состояние.
        /// </summary>
        string startingState;
        /// <summary>
        /// Поле, задающее единое конечное состояние.
        /// </summary>
        string uniqueFinal;
        /// <summary>
        /// Поле, задающее автомат, у которого удаляются состояния.
        /// </summary>
        GraphBuilder graph;
        /// <summary>
        /// Конструктор шага удаления состояния.
        /// </summary>
        /// <param name="changhedGraph">текущее состояние графа</param>
        /// <param name="list">список неудаляемых состояний</param>
        /// <param name="headState">начальное состояние</param>
        public StateRemoval(GraphBuilder changhedGraph,List<string> list, string headState, string unqueFinal)
        {
            uniqueFinal = unqueFinal;
            startingState = headState;
            untouchedStatesList = list;
            graph = changhedGraph;
        }
        /// <summary>
        /// Метод, который проверяет, является ли состояние конечным или начальным.
        /// </summary>
        /// <param name="vertexName">имя состояния</param>
        /// <returns></returns>
        public bool isUntouched(string vertexName)
        {
            return untouchedStatesList.Exists(x => x == vertexName);
        }
        /// <summary>
        /// Метод, добавляющий в автомат единое конечное состояние.
        /// </summary>
        /// <param name="uniqueFinal">Имя единого конечного состояния.</param>
        /// <returns></returns>
        public GraphBuilder TransformGraph(out string uniqueFinal)
        {
            graph.UserAddFinalVertix(uniqueFinal = RandomVertexName());
            foreach(string vertex in untouchedStatesList)
            {
                if (vertex != String.Empty && vertex != startingState)
                {
                    graph.UserRemoveFinalVertix(vertex);
                    graph.UserAddEdge(vertex, uniqueFinal, "&");
                }
            }
            return graph;
        }
        /// <summary>
        /// Метод, проверяющий, остались ли в автомате неконечные состояния, при положительном результате возвращает имя состояния, которое необходимо удалить.
        /// </summary>
        /// <param name="name">Имя неконечного состояния.</param>
        /// <returns>Наличие неконечных состояния.</returns>
        public bool NonFinalLeft(out string name)
        {
            bool result = false;
            name = "";
            graph.ConcatSelfLoops();
            Dictionary<string, int> income = new Dictionary<string, int>();//ключ - имя состояния, значение - длины всех регулярных выражений, задающих переходы входящих в состояние дуг
            Dictionary<string, int> outcome = new Dictionary<string, int>();//ключ - имя состояния, значение - длины всех регулярных выражений, задающих переходы исходящих из состояния дуг
            Dictionary<string, int> inrange = new Dictionary<string, int>();//ключ- имя состояния, значение - количество входящих в состояние дуг
            Dictionary<string, int> outrange = new Dictionary<string, int>();//ключ- имя состояния, значение - количество исходящих из состояния дуг
            Dictionary<string, int> self = new Dictionary<string, int>();//ключ - имя состояния, значение - длина регулярного выражения, задающего петлю
            Dictionary<string, int> damage = new Dictionary<string, int>();//ключ - имя состояния, значение - урон, который нанесет удаление состояния регулярному выражению, задающему автомат
            foreach(var edge in graph.myGraph.Edges)//обход всех дуг конечного автомата и заполнение словарей
            {
                if (!self.Keys.Contains(edge.Source))
                    self.Add(edge.Source, 0);
                if (!self.Keys.Contains(edge.Target))
                    self.Add(edge.Target, 0);
                if (!income.Keys.Contains(edge.Target))
                {
                    income.Add(edge.Target, 0);
                    inrange.Add(edge.Target, 0);
                }
                if (!income.Keys.Contains(edge.Source))
                {
                    income.Add(edge.Source, 0);
                    inrange.Add(edge.Source, 0);
                }
                if (!outcome.Keys.Contains(edge.Source))
                {
                    outcome.Add(edge.Source, 0);
                    outrange.Add(edge.Source, 0);
                }
                if (!outcome.Keys.Contains(edge.Target))
                {
                    outcome.Add(edge.Target, 0);
                    outrange.Add(edge.Target, 0);
                }
                if (edge.Source == edge.Target)
                    self[edge.Source] += edge.Tag.Length;
                else
                {
                        income[edge.Target] += edge.Tag.Length;
                        inrange[edge.Target]++;
                        outcome[edge.Source] += edge.Tag.Length;
                        outrange[edge.Source]++;
                }
            }
            foreach(string key in income.Keys)
                damage.Add(key, income[key] * outcome[key] + inrange[key] * outrange[key] * self[key]);
            int min = 1000000000;
            foreach(string key in damage.Keys)//поиск состояния с минимальным уроном
            {
                if (!isUntouched(key))
                {
                    if (damage[key] < min)
                    {
                        min = damage[key];
                        name = key;
                    }
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// Метод, реализующий алгоритм удаления недопускающего состояния.
        /// </summary>
        /// <param name="nameOfVertex">Имя удаляемого состояния.</param>
        /// <returns></returns>
        public GraphBuilder RemoveNonFinalState(string nameOfVertex)
        {
            int numOfOutEdges;//количество исходящих из состояния дуг
            int numOfInEdges;//количество входящих в состояние дуг
            string selfEdge = String.Empty;//петля
            bool existInEdges=false;//индикатор, есть ли входящие дуги
            enteringEdges = new List<InEdgeList>();//список входящих дуг
            exitingEdges = new List<OutEdgeList>();//список исходящих дуг
            if (graph.myGraph.OutEdges(nameOfVertex).Count() == 0)//исключение, если из неконечного состояния нет исходящих дуг 
                throw new Exception("The automata is not finite!");
            foreach (var edge in graph.myGraph.OutEdges(nameOfVertex))//формируем список исходящих из состояния дуг
            {
                if (edge.Target == nameOfVertex)
                    selfEdge = edge.Tag;
                else
                    exitingEdges.Add(new OutEdgeList { EdgeLabel = edge.Tag, TargetVertex = edge.Target });
            }
            foreach(var edge in graph.myGraph.Edges)//формируем список входящих в состояние дуг
            {
                if(edge.Target==nameOfVertex&&edge.Source!=nameOfVertex)
                {
                        enteringEdges.Add(new InEdgeList { EdgeLabel = edge.Tag, SourceVertex = edge.Source });
                        existInEdges = true;
                }
            }
            if (!existInEdges)//исключение, если в состояние нет входящих дуг 
                throw new Exception("The automata is not finite!");
            numOfOutEdges = exitingEdges.Count;
            numOfInEdges = enteringEdges.Count;
            List<string> numberOfUniqChangedEdges = new List<string>();//список получившихся регулярных выражений
            Dictionary<string,int> prefixes = new Dictionary<string, int>();//словарь префиксов, ключ которого - регулярное выражение, задающее префикс, а значение - количество повторений префикса
            for (int enterIndex=0;enterIndex<numOfInEdges;enterIndex++)//формируем регулярные выражения для всех пар (входящая дуга) - (исходящая дуга)
                                                                //это выражения, соединяющие состояния, связанные удаляемым
                for(int exitIndex=0;exitIndex<numOfOutEdges;exitIndex++)
                {
                    graph.ConcatSelfLoops();
                    string label = "";//регулярное выражение, для текущей пары (входящая дуга) - (исходящая дуга)
                    bool existsEdge=false;//индикатор, есть ли дуга, связывающая состояния
                    TaggedEdge<string, string> edge;//дуга, соединяющая 
                    string source = enteringEdges[enterIndex].SourceVertex;//состояние,из которого есть переход в удаляемое
                    string target = exitingEdges[exitIndex].TargetVertex;//состояние,в которое есть переход из удаляемого
                    if (graph.myGraph.TryGetEdge(source, target, out edge) && !(source == startingState & target == uniqueFinal))//проверяем, есть ли дуга, связывающая состояния source и target
                    {
                            label = edge.Tag + "+";
                            existsEdge = true;
                    }
                    if (label != "" && label.Last() != '+')
                            label += '+';    
                    string addedLabel = "";//регулярное выражение, получившееся после удаления текущего состояния
                    if (enteringEdges[enterIndex].EdgeLabel.Contains("+"))
                         addedLabel += "(" + enteringEdges[enterIndex].EdgeLabel + ")";    
                    else 
                         addedLabel += enteringEdges[enterIndex].EdgeLabel;//добавляем в регулярное выражение символ, по которому осуществляется переход из состояния source в удаляемое   
                        if (selfEdge != String.Empty)//если существует петля, добавляем ее к регулярному выражению
                        {
                            if (addedLabel != "" && addedLabel.Last() != '.')
                                addedLabel += '.';
                            addedLabel += "("+selfEdge+")*";
                        }  
                        if(!prefixes.Keys.Contains(addedLabel)&&addedLabel.Length>2)//если префикс не был добавлен в словарь ранее, добавляем его
                            prefixes.Add(addedLabel,0);
                        if (addedLabel != "" && addedLabel.Last() != '.')
                            addedLabel += '.';
                        if (exitingEdges[exitIndex].EdgeLabel.Contains("+"))//добавляем к регулярному выражению конкатенацию с переходом исходящей дуги
                            addedLabel += "(" + exitingEdges[exitIndex].EdgeLabel + ")";
                        else
                            addedLabel += exitingEdges[exitIndex].EdgeLabel;
                        if (existsEdge)//если дуга уже есть, удаляем ее
                            graph.myGraph.RemoveEdge(edge);
                        //добавляем дугу с получившимся регулярным выражением
                        graph.UserAddEdge(enteringEdges[enterIndex].SourceVertex, exitingEdges[exitIndex].TargetVertex, label+ addedLabel);
                    if (!numberOfUniqChangedEdges.Contains(label + addedLabel))
                        numberOfUniqChangedEdges.Add(label + addedLabel);
                    else if(!graph.finalDefinitions.Values.Contains(label+ addedLabel))
                    {
                     int num = graph.finalDefinitions.Values.Count;
                            string str = ((char)(num % 26 + 65)).ToString();
                            if (num >= 26)
                                str += num / 26;
                        graph.finalDefinitions.Add(str, label + addedLabel);       
                    }
                }
                graph.ConcatSelfLoops();
                graph.myGraph.RemoveVertex(nameOfVertex);
            DefineSubprocesses();
            DefinePrefixes(prefixes);
            return graph;
        }
        /// <summary>
        /// Метод извлечения подпроцессов по префиксам.
        /// </summary>
        /// <param name="prefixes">Словарь префиксов.</param>
        void DefinePrefixes(Dictionary<string, int> prefixes)
        {
            List<string> keys = SortByLength(new List<string>(prefixes.Keys));//сортируем список префисксов по убыванию их длины
            //замена по подпроцессам
            foreach (var edge in graph.myGraph.Edges)
            {
                string newLabel = "";
                string[] stringArray = edge.Tag.Split('+');
                int i = 0;
                foreach (string str in stringArray)
                {
                    i++;
                    if (graph.finalDefinitions.Values.Contains(str))
                    {
                        foreach (string key in graph.finalDefinitions.Keys)
                            if (graph.finalDefinitions[key] == str)
                                newLabel += key;
                    }
                    else
                        newLabel += str;
                    if (i != stringArray.Length)
                        newLabel += "+";
                }
                edge.Tag = newLabel;
                foreach (string prefix in keys)//подсчитывается количество вхождений префиксов в состав перехода
                    prefixes[prefix] += CountOccurance(newLabel, prefix);
            }
            Dictionary<string, string> frequentPrefixes = new Dictionary<string, string>();//словарь часто встречающиеся префиксов
            foreach (string prefix in keys)
                if (prefixes[prefix] > 1)
                {
                    int num = graph.finalDefinitions.Values.Count + frequentPrefixes.Count;
                    string strVal = ((char)(num % 26 + 65)).ToString();
                    if (num >= 26)
                        strVal += num / 26;
                    frequentPrefixes.Add(strVal, prefix);
                }
            //замена по префиксам
            foreach (var edge in graph.myGraph.Edges)
                foreach (string prefix in frequentPrefixes.Keys)
                    edge.Tag = RemoveIdentic(edge.Tag, frequentPrefixes[prefix], prefix);
            foreach (string key in frequentPrefixes.Keys)
                graph.finalDefinitions.Add(key, frequentPrefixes[key]);
        }
        /// <summary>
        /// Метод извлечения подпроцессов в составе переходов на дугах конечного автомата.
        /// </summary>
        void DefineSubprocesses()
        {
            List<string> numberOfUniqParts = new List<string>();//список подпроцессов
            foreach (var edge in graph.myGraph.Edges)
            {
                foreach (string key in graph.finalDefinitions.Keys)//заменяем подпроцессы всех переходов на ключевые символы подпроцессов
                    if (graph.finalDefinitions[key] == edge.Tag)
                        edge.Tag = key;
                string[] strArray = (edge.Tag).Split('+');
                List<string> strArr = new List<string>();
                int i = 0;
                while (i < strArray.Length)//находим все подпроцессы, входящие в состав данного перехода
                {
                    char[] symbs = strArray[i].ToCharArray();
                    int open = 0;
                    int close = 0;
                    foreach (char c in symbs)
                        if (c == '(')
                            open++;
                        else if (c == ')')
                            close++;
                    if (open == close)
                        strArr.Add(strArray[i]);
                    else
                    {
                        string makeNormal = strArray[i];
                        do
                        {
                            i++;
                            makeNormal += "+" + strArray[i];
                            open = 0;
                            close = 0;
                            symbs = makeNormal.ToCharArray();
                            foreach (char c in symbs)
                                if (c == '(')
                                    open++;
                                else if (c == ')')
                                    close++;
                        } while (open != close);
                        strArr.Add(makeNormal);
                    }
                    i++;
                }
                foreach (string str in strArr)//добавляем все подпроцессы, входящие в состав данного перехода, длина которых более одного символа, в словарь подпроцессов
                    if (str.Length != 1)
                    {
                        if (!numberOfUniqParts.Contains(str))
                            numberOfUniqParts.Add(str);
                        else if (!graph.finalDefinitions.Values.Contains(str))
                        {
                            int num = graph.finalDefinitions.Values.Count;
                            string strVal = ((char)(num % 26 + 65)).ToString();
                            if (num >= 26)
                                strVal += num / 26;
                            graph.finalDefinitions.Add(strVal, str);
                        }
                    }
            }
        }
        /// <summary>
        /// Метод, подсчитывающий количество повторений подстроки в строке.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="substr"></param>
        /// <returns></returns>
        int CountOccurance(string str, string substr)
        {
            int count = (str.Length - str.Replace(substr, "").Length) / substr.Length;
            return count;
        }
        /// <summary>
        /// Метод, сортирующий список строк по убыванию их длины.
        /// </summary>
        /// <param name="list">Сортируемый список.</param>
        /// <returns>Отсортированный список.</returns>
        List<string> SortByLength(List<string> list)
        {
            List<string> result = new List<string>();
            int max = 0;
            do
            {
                max = 0;
                foreach (string str in list)
                    if (str.Length > max)
                        max = str.Length;
                bool found = false;
                do
                {

                    found = false;
                    foreach (string str in list)
                    {
                        if (str.Length == max)
                        {
                            found = true;
                            list.Remove(str);
                            result.Add(str);
                            break;
                        }
                    }
                } while (found == true);
            } while (list.Count > 0);
            return result;
        }
        /// <summary>
        /// Метод, заменяющий подпроцессы в регулярном выражении на их ключевые символы.
        /// </summary>
        /// <param name="str">Регулярное выражение.</param>
        /// <param name="substr">Подпроцесс.</param>
        /// <param name="change">Ключевой символ подпроцесса.</param>
        /// <returns>Измененное регулярное выражение.</returns>
        string RemoveIdentic(string str, string substr, string change)
        {
            string cleanedStr="";
            if (str.Contains(substr))
                do
                {
                    int index = str.IndexOf(substr);
                    cleanedStr += str.Substring(0, index)+change;
                    str = str.Remove(0, index + substr.Length);
                } while (str.Contains(substr));
            cleanedStr += str;
            return cleanedStr ;
        }
        /// <summary>
        /// Поиск свободного имени для состояния.
        /// </summary>
        /// <returns>Имя состояния.</returns>
            public string RandomVertexName()
        {
            string name;
            name = rand.Next(65, 97).ToString();
            if (graph.myGraph.Vertices.Contains(name))
                do
                    name += rand.Next(65, 97).ToString();
                while (!graph.myGraph.Vertices.Contains(name));
            return name;
        }
        /// <summary>
        /// Метод, удаляющий лишние скобки в регулярных выражениях.
        /// </summary>
        /// <param name="expressions">Список регулярных выражений, к которым требуется применить алгоритм.</param>
        /// <returns>Список измененных регулярных выражений.</returns>
        public List<string> DeleteBrackets(List<string> expressions)
        {
            int count;
            for (int i=0;i<expressions.Count;i++)
            {
                expressions[i].Remove(0,1);
                expressions[i].Remove(expressions[i].Length - 1,1);
            }
            count = expressions.Count;
            for(int index=0;index<count;index++)
            {
                foreach(string expression in expressions)
                {
                    if(expression=="")
                    {
                        expressions.Remove(expression);
                        break;
                    }
                }
            }
            return expressions;
        }
        /// <summary>
        /// Метод, осуществляющий финальное преобразование конечного автомата в регулярное выражение
        /// </summary>
        /// <param name="graph">Конечный автомат(представлен как начальное и конечное состояние, множество переходов).</param>
        /// <returns>Регулярные выражения.</returns>
        public List<string> FinalRemoval(GraphBuilder graph)
        {
            string regularExpression = "";
            int count = 0;
            List<string> expressions = new List<string>();
            foreach (var edge in graph.myGraph.OutEdges(startingState))
            {
                regularExpression = Change(edge.Tag.Replace('\n', ' '));
                count++;
                expressions.Add(regularExpression);

            }
            expressions = DeleteBrackets(expressions);
            for(int index=0;index<expressions.Count;index++)
            {
                if (index != count - 1)
                    expressions[index] += "+";
            }
            return expressions;
        }
        /// <summary>
        /// Метод, удаляющий пустые переходы.
        /// </summary>
        /// <param name="expression">Изменяемое выражение.</param>
        /// <returns>змененное выражение.</returns>
        public string Change(string expression)
        {
            string result="";
            char[] expArr = expression.ToCharArray();
            foreach(char c in expArr)
            {
                if (c == '&' && result[result.Length - 1] == '.')
                    result = result.Remove(result.Length - 1);
                else if (c == '+' && result[result.Length - 1] == '&')
                {
                    result = result.Remove(result.Length - 1);
                    result += (char)400;
                    result += c;
                }
                else
                    result += c;
            }
            return result;
        }
    }
}
